#include <Misc.au3>
#include <Array.au3>
#include <WindowsConstants.au3>
#include <EditConstants.au3>
#include <File.au3>
#include <WinAPI.au3>
#include <GDIPlus.au3>

Opt("GUIOnEventMode", 1)

Global $CurCommand, $Read, $Commands[13], $CommandsDis[13], $Right, $CurCommandParameters, $CurCommandString, $Curpath = @ScriptDir, $Loesung
Global $dll = DllOpen("user32.dll")
Global $title = "Console"

$Commands[0] = "HELP"
$Commands[1] = "EXIT"
$Commands[2] = "MSGBOX"
$Commands[3] = "CLEAR"
$Commands[4] = "COLOUR"
$Commands[5] = "HIDE"
$Commands[6] = "CREATE_FILE"
$Commands[7] = "CD"
$Commands[8] = "SOLVE"
$Commands[9] = "EXECUTE"
$Commands[10] = "DRAWLINE"
$Commands[11] = "DRAWIMAGE"
$Commands[12] = "GRAPHICSCLEAR"

$CommandsDis[0] = "Shows a List of all commands/E/tParameter 1 = Command (optional)"
$CommandsDis[1] = "Exit the console"
$CommandsDis[2] = "Shows a massage/E/tParameter 1 = Flag/E/tParameter 2 = Title (string)/E/tParameter 3 = Text (string)"
$CommandsDis[3] = "Clears the Console"
$CommandsDis[4] = "Changes the font and background colour/E/tParameter 1 = font colour (HEX)/E/tParameter 2 = background colour (HEX) (optional)"
$CommandsDis[5] = "Hides the Console until its activated again"
$CommandsDis[6] = "Creates a file or overwrites existing File/Ewith previous & the File gets written to directionary"
$CommandsDis[7] = "Changes the directory/E/tParameter 1 = New directory"
$CommandsDis[8] = "/ESolves a Calculation./ESet a prefix in front of the Calculation to claculate with the Solve of the Calciulation before. (Please Correct my English ;-) )"
$CommandsDis[9] = "Executes a path with the ShellExecute Engine/EIf no parameters are denoted the last changed/created/moved File will open."
$CommandsDis[10] = "Draws a line/E/tParameter 1 = Startpoint X/E/tParameter 2 = Startpoint Y/E/tParameter 3 = Endpoint X/E/tParameter 4 = Endpoint Y/E/tParameter 5 = color of the line (string) (optional)/E/tParameter 6 = the width of the line (optional)"
$CommandsDis[11] = "Draws an image/E/tParameter 1 = Full path to the Image (string)/E/tParameter 2 = X Kooardinate/E/tParameter 3 = Y Kooardinate"
$CommandsDis[12] = "Clears the graphic"

$Console = _ConsoleCreate($title)
_ConsoleWrite($Console, "Welcome")
$hwnd = GUICreate("", @DesktopWidth, @DesktopHeight, 0, 0, 0, BitOR($WS_EX_LAYERED, $WS_EX_TOPMOST), $Console[0])

_GDIPlus_Startup()
$graphics = _GDIPlus_GraphicsCreateFromHWND($hwnd)
$bitmap = _GDIPlus_BitmapCreateFromGraphics(@DesktopWidth, @DesktopHeight, $graphics)
$backbuffer = _GDIPlus_ImageGetGraphicsContext($bitmap)
$constantpen = _GDIPlus_PenCreate()
$ScreenDc = _WinAPI_GetDC($hWnd)
$dc = _WinAPI_CreateCompatibleDC($ScreenDc)

; _WinAPI_UpdateLayeredWindow parameters
$tSize = DllStructCreate($tagSIZE)
$pSize = DllStructGetPtr($tSize)
DllStructSetData($tSize, "X", @DesktopWidth)
DllStructSetData($tSize, "Y", @DesktopHeight)
$tSource = DllStructCreate($tagPOINT)
$pSource = DllStructGetPtr($tSource)
$tBlend = DllStructCreate($tagBLENDFUNCTION)
$pBlend = DllStructGetPtr($tBlend)
DllStructSetData($tBlend, "Alpha", 255)
DllStructSetData($tBlend, "Format", 1)
$tPoint = DllStructCreate($tagPOINT)
$pPoint = DllStructGetPtr($tPoint)
DllStructSetData($tPoint, "X", 0)
DllStructSetData($tPoint, "Y", 0)
updatewin()

GUISetState()
WinActivate($Title)

While 1
    If _IsPressed("0D", $dll) And WinActive($title) Then
        $array = StringSplit(GUICtrlRead($Console[1]), @CRLF)
        $CurCommand = $array[Ubound($array) - 1]
        $CurCommandParameters = StringSplit($CurCommand, " ")
        $CurCommandString = StringSplit($CurCommand, '"')
        For $i = 1 To $array[0] - 1 Step 2
            $Read &= $array[$i] & @CRLF
        Next
        GUICtrlSetData($Console[1], $Read & "> " & $CurCommand & @CRLF)
        For $i = 0 To UBound($Commands) - 1
            If $CurCommandParameters[1] = $Commands[$i] Then
                Call("_ConsoleCommand_" & $Commands[$i])
                $Right = True
                ExitLoop
            Else
                $Right = False
            EndIf
        Next
        If Not $Right Then
            _ConsoleWrite($Console, "unknown Command/EType HELP to list all commands")
        EndIf
        While _IsPressed("0D", $dll) And WinActive($title)
            Sleep(100)
        WEnd
        $Read = ""
    EndIf
    Sleep(30)
WEnd

Func _ConsoleCommand_SOLVE()
If UBound($CurCommandParameters) = 3 Then
$RegExString = StringRegExp($CurCommandParameters[2],"^.",3)

If IsDeclared("Loesung") and ($RegExString[0] =  "+" or $RegExString[0] = "-" or $RegExString[0] = "*" or $RegExString[0] = "/") Then
        $Loesung = Execute($Loesung & $CurCommandParameters[2])
    If IsNumber($Loesung) Then
        _ConsoleWrite($Console, $Loesung)
    Else
        _ConsoleWrite($Console, "Wrong arguments, solve: " & $Loesung)
    EndIf

    Else


    $Loesung = Execute($CurCommandParameters[2])
    If IsNumber($Loesung) Then
        _ConsoleWrite($Console, $Loesung)
    Else
        _ConsoleWrite($Console, "Wrong arguments, solve: " & $Loesung)
    EndIf
    EndIf
Else
    _ConsoleWrite($Console, "Parameter Error")
EndIf

EndFunc

Func _ConsoleCommand_EXECUTE()
    If UBound($CurCommandParameters) > 3 Then
        _ConsoleWrite($Console, "Parameter Error")
    ElseIf UBound($CurCommandParameters) = 2 Then
        If IsDeclared("CreatingPath") Then
            ShellExecute($CreatingPath)
        Else
            _ConsoleWrite($Console,"No File found")
        EndIf

    Else
        ShellExecute($CurCommandParameters[2])
    EndIf
EndFunc


Func _ConsoleCommand_CD()
    If UBound($CurCommandString) > 3 Then
        $CurPath = $CurCommandString[2]
    Else
        _ConsoleWrite($Console, "Parameter Error")
    EndIf
EndFunc

Func _ConsoleCommand_CREATE_FILE()
;~     MsgBox(0,"",$CurCommandParameters[2])
If UBound($CurCommandParameters) = 3 Then $StringREG = StringRegExp($CurCommandParameters[2],"^.",3)
If UBound($CurCommandParameters) > 3 Then
    _ConsoleWrite($Console, "Parameter Error")
ElseIf UBound($CurCommandParameters) = 3 and $StringREG[0] = "&" Then
    $Datei = StringReplace($CurCommandParameters[2],"&","")
    If StringRegExp($CurPath ,".$",3) = "" Then
        $CreatingPath = $CurPath & $Datei
        _FileCreate($CurPath & $Datei)
    Else
        _FileCreate($CurPath & "" & $Datei)
        $CreatingPath = $CurPath & "" & $Datei
    EndIf

    If Not FileExists($CreatingPath) or not @error = 0 Then
        Switch @error
            Case 0
                _ConsoleWrite($Console,"unknown Error - check path")
            Case 1
                _ConsoleWrite($Console,"Error - cant open existing file")
            Case 2
                _ConsoleWrite($Console,"Error - cant write to existing file")
        EndSwitch

        Else
            _ConsoleWrite($Console,"Sucsess")
        EndIf
ElseIf UBound($CurCommandParameters) = 3 Then
    $CreatingPath = $CurCommandParameters[2]
    _FileCreate($CurCommandParameters[2])
    If Not FileExists($CurPath) or not @error = 0 Then
        Switch @error
            Case 0
                _ConsoleWrite($Console,"unknown Error - check path")
            Case 1
                _ConsoleWrite($Console,"Error - cant open existing file")
            Case 2
                _ConsoleWrite($Console,"Error - cant write to existing file")
        EndSwitch

        Else
            _ConsoleWrite($Console,"Sucsess")
        EndIf
    Else
        _ConsoleWrite($Console, "Parameter Error")
EndIf

EndFunc


Func _ConsoleCommand_HIDE()
    _WinSetSize($Console[0],660, 30, 10, 4)
    GUISetState(@SW_SHOWMINIMIZED)
    Do
        Sleep(20)
    Until WinActive($Title)
    _WinSetSize($Console[0], 660, 329, 10, 5)
EndFunc

Func _ConsoleCommand_COLOUR()
    If UBound($CurCommandParameters) = 3 Then
        GUICtrlSetColor($Console[1], "0x" & $CurCommandParameters[2])
    ElseIf UBound($CurCommandParameters) = 4 Then
        GUICtrlSetColor($Console[1], "0x" & $CurCommandParameters[2])
        GUICtrlSetBkColor($Console[1], "0x" & $CurCommandParameters[3])
    Else
        _ConsoleWrite($Console, "Parameter Error")
    EndIf
EndFunc

Func _ConsoleCommand_CLEAR()
    GUICtrlSetData($Console[1], "")
EndFunc

Func _ConsoleCommand_MSGBOX()
    If Not UBound($CurCommandParameters) < 5 And UBound($CurCommandString) = 6 Then
        MsgBox(Number($CurCommandParameters[2]), $CurCommandString[2], $CurCommandString[4])
    Else
        _ConsoleWrite($Console, "Parameter Error")
    EndIf
EndFunc

Func _ConsoleCommand_Exit()
    _WinSetSize($Console[0], 660, 0, 10, 5)
    Exit
EndFunc

Func _ConsoleCommand_HELP()
    If UBound($CurCommandParameters) = 2 Then
        For $i = 0 To UBound($Commands) - 1
            _ConsoleWrite($Console, $Commands[$i])
        Next
    ElseIf UBound($CurCommandParameters) = 3 Then
        For $i = 0 To UBound($Commands) - 1
            If $CurCommandParameters[2] = $Commands[$i] Then
                _ConsoleWrite($Console, $Commands[$i] & "/t" & $CommandsDis[$i])
                ExitLoop
            EndIf
        Next
    Else
        _ConsoleWrite($Console, "Parameter Error")
    EndIf
EndFunc

Func _SM_SetVisualStyle($b_Enable)
    Return DllCall("uxtheme.dll", "none", "SetThemeAppProperties", "int", $b_Enable)
EndFunc

Func _ConsoleCreate($Title)
    Local $Console[2]
    _SM_SetVisualStyle(False)
    $Console[0] = GUICreate($Title, 654, 0, @DesktopWidth- 660, 0, Default)
        GUISetOnEvent(-3, "_ConsoleCommand_Exit")
        GUISetBkColor(0x000000)
        $Console[1] = GUICtrlCreateEdit("", 0, 0, 657, 297, BitOR($ES_AUTOVSCROLL, $WS_VSCROLL))
            GUICtrlSetBkColor($Console[1], 0x000000)
            GUICtrlSetColor($Console[1], 0xC0C0C0)
            GUICtrlSetFont($Console[1], 10, Default, Default, "Lucida Console")
    GUISetState(@SW_SHOW)
    _WinSetSize($Console[0], 660, 329, 10, 5)
    Return $Console
Endfunc

Func _ConsoleWrite($Console, $String)
    Local $timer, $split
    $String = StringReplace($String, "/E", @CRLF)
    $String = StringReplace($String, "/t", @TAB)
    $split = StringSplit($String, "")
    $timer = TimerInit()
    For $i = 1 To $split[0]
        Sleep(15)
        GUICtrlSetData($Console[1], GUICtrlRead($Console[1]) & $split[$i])
    Next
    GUICtrlSetData($Console[1], GUICtrlRead($Console[1]) & @CRLF)
EndFunc

Func _WinSetSize($hWnd, $iWidth, $iHeigth, $iSleep = -1,$iStep = 4);by Gta Spider
    If $iSleep < 0 Then Return WinMove($hWnd,"",Default,Default,$iWidth,$iHeigth)
    Local $i, $iStepW = $iStep, $iStepH = $iStep,$iSin,$i2,$iLastI
    Local $aWinPos = WinGetPos($hWnd)
    If $iWidth < $aWinPos[2] Then $iStepW *= -1
    If $iHeigth < $aWinPos[3] Then $iStepH *= -1
    Local $iDegToRad = 3.14159265358979/180
    Local $iSinMulW = ($iWidth - $aWinPos[2]) / 2.5
    Local $iSinMulH = ($iHeigth - $aWinPos[3]) / 2.5
    If $iSinMulW < 0 Then $iSinMulW *= -1
    If $iSinMulH < 0 Then $iSinMulH *= -1
    If $iWidth <> $aWinPos[2] Then
        For $i = $aWinPos[2] To $iWidth Step $iStepW
            $iLastI = $i
            $iSin = $i - $aWinPos[2]
            $iSin = Sin($iDegToRad * (180*$iSin/($iWidth - $aWinPos[2])))
            WinMove($hWnd,"",Default,Default,$i + ($iSin * $iSinMulW),Default)
            Sleep($iSleep)
        Next
        If Mod($iWidth - $aWinPos[2],$iStep) Then
            If $iStepW < 0 Then
                $iStepW = -1
            Else
                $iStepW = 1
            EndIf
            For $i = $iLastI -1 To $iWidth Step $iStepW
                WinMove($hWnd,"",Default,Default,$i,Default)
            Next
        EndIf
    EndIf
    If $iHeigth <> $aWinPos[3] Then
        For $i = $aWinPos[3] To $iHeigth Step $iStepH
            $iLastI = $i
            $iSin = $i - $aWinPos[3]
            $iSin = Sin($iDegToRad*(180*$iSin/($iHeigth - $aWinPos[3])))
            WinMove($hWnd,"",Default,Default,Default,$i + ($iSin * $iSinMulH))
            Sleep($iSleep)
        Next
        If Mod($iHeigth - $aWinPos[3],$iStep) Then
            If $iStepH < 0 Then
                $iStepH = -1
            Else
                $iStepH = 1
            EndIf
            For $i = $iLastI -1 To $iHeigth Step $iStepH
                WinMove($hWnd,"",Default,Default,Default,$i)
            Next
        EndIf
    EndIf
EndFunc


Func _ConsoleCommand_DRAWLINE()
    Local $pen, $color
    If UBound($CurCommandParameters) = 6 Then
        _GDIPlus_GraphicsDrawLine($backbuffer, $CurCommandParameters[2], $CurCommandParameters[3], $CurCommandParameters[4], $CurCommandParameters[5], $constantpen)
        if @error Then _ConsoleWrite($Console, "Error")
        updatewin()
    ElseIf UBound($CurCommandParameters) = 7 Then
        $color = "0xFF"&$CurCommandString[2]
        $pen = _GDIPlus_PenCreate($color)
        if @error Then _ConsoleWrite($Console, "Error")
        _GDIPlus_GraphicsDrawLine($backbuffer, $CurCommandParameters[2], $CurCommandParameters[3], $CurCommandParameters[4], $CurCommandParameters[5], $pen)
        if @error Then _ConsoleWrite($Console, "Error")
        _GDIPlus_PenDispose($pen)
        updatewin()
    ElseIf UBound($CurCommandParameters) = 8 Then
        $color = "0xFF"&$CurCommandString[2]
        $pen = _GDIPlus_PenCreate($color, $CurCommandParameters[7])
        _GDIPlus_GraphicsDrawLine($backbuffer, $CurCommandParameters[2], $CurCommandParameters[3], $CurCommandParameters[4], $CurCommandParameters[5], $pen)
        if @error Then _ConsoleWrite($Console, "Error")
        _GDIPlus_PenDispose($pen)
        if @error Then _ConsoleWrite($Console, "Error")
        updatewin()
    Else
        _ConsoleWrite($Console, "Parameter Error")
    EndIf
EndFunc

Func _ConsoleCommand_DRAWIMAGE()
    If UBound($CurCommandParameters) = 5 Then
        _GDIPlus_GraphicsDrawImage($backbuffer, _GDIPlus_ImageLoadFromFile($CurCommandString[2]), $CurCommandParameters[3], $CurCommandParameters[4])
        updatewin()
    Else
        _ConsoleWrite($Console, "Parameter Error")
    EndIf

    EndFunc

Func _ConsoleCommand_GRAPHICSCLEAR()
    DllCall($ghGDIPDll, "int", "GdipGraphicsClear", "handle", $backbuffer, "dword", "0x00000000")
    updatewin()
EndFunc


Func updatewin()
    $gdibitmap = _GDIPlus_BitmapCreateHBITMAPFromBitmap($bitmap)
    _WinAPI_SelectObject($dc, $gdibitmap)
    _WinAPI_UpdateLayeredWindow($hWnd, $ScreenDc, 0, $pSize, $dc, $pSource, 0, $pBlend, 2)
    EndFunc