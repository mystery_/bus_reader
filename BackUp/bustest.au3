;#AutoIt3Wrapper_Res_File_Add=test_1.txt, rt_rcdata, TEST_TXT_1




;#include <MsgBoxConstants.au3>
#include <ButtonConstants.au3>
#include <EditConstants.au3>
#include <GUIConstantsEx.au3>
#include <GUIListBox.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>
#include <GuiStatusBar.au3>
#include "resources.au3"
#include "CommMG.au3"


Opt("TrayMenuMode", 1)
Opt("TrayMenuMode", 0)
Opt("TrayMenuMode", 0)
;Opt("GUIOnEventMode", 1)

#region GUI
$Form1 = GUICreate("Form1", 613, 438, 192, 124)
$portbox = GUICtrlCreateList("", 40, 48, 81, 84)
$Button2 = GUICtrlCreateButton("Clear I/Buffer", 280, 48, 75, 25)
$singlestep = GUICtrlCreateButton("Single", 280, 120, 75, 25)
$Button4 = GUICtrlCreateButton("test345", 280, 185, 75, 25)
$baudinput = GUICtrlCreateInput("250000", 168, 56, 73, 21, BitOR($GUI_SS_DEFAULT_INPUT, $ES_RIGHT, $ES_NUMBER))
GUICtrlSetBkColor(-1, 0xBFCDDB)
$connectbutton = GUICtrlCreateButton("Connect", 168, 104, 75, 25, $BS_DEFPUSHBUTTON)
$startstop = GUICtrlCreateButton("Start/Stop", 280, 88, 75, 25, $GUI_DISABLE)
GUIStartGroup()
$Checkbox1 = GUICtrlCreateCheckbox("Clear on Single", 40, 200, 97, 17)
GUICtrlSetState($Checkbox1, $GUI_CHECKED)
$Checkbox2 = GUICtrlCreateCheckbox("Checkbox2", 40, 224, 97, 17)
$bin = GUICtrlCreateRadio("BIN", 176, 200, 100, 17)
$hex = GUICtrlCreateRadio("HEX", 176, 224, 100, 17)
GUICtrlSetState($hex, $GUI_CHECKED)
$Checkbox3 = GUICtrlCreateCheckbox("Checkbox3", 40, 248, 97, 17)
$Radio3 = GUICtrlCreateRadio("Radio3", 176, 248, 100, 17)
GUIStartGroup()
$Group1 = GUICtrlCreateGroup("Group1", 24, 176, 241, 105)
GUICtrlCreateGroup("", -99, -99, 1, 1)
$Group2 = GUICtrlCreateGroup(" Main Settings ", 24, 24, 241, 129)
$Label1 = GUICtrlCreateLabel("Baud", 128, 56, 32, 18)
GUICtrlSetFont(-1, 11, 400, 0, "MS Sans Serif")
GUICtrlSetResizing(-1, $GUI_DOCKAUTO)
$refreshlist = GUICtrlCreateButton("Button1", 128, 80, 27, 25, $BS_BITMAP)
GUICtrlSetImage(-1, "C:\Users\JannisDesktop\Desktop\RETRY.BMP", -1)

GUICtrlCreateGroup("", -99, -99, 1, 1)
$Pic1 = GUICtrlCreatePic("C:\Users\JannisDesktop\Desktop\Unbenannt.JPG", 376, 24, 217, 137, BitOR($GUI_SS_DEFAULT_PIC, $WS_BORDER))
$StatusBar1 = _GUICtrlStatusBar_Create($Form1)
Dim $StatusBar1_PartsWidth[3] = [50, 200, -1]
_GUICtrlStatusBar_SetParts($StatusBar1, $StatusBar1_PartsWidth)
_GUICtrlStatusBar_SetText($StatusBar1, "", 0)
_GUICtrlStatusBar_SetText($StatusBar1, "", 1)
_GUICtrlStatusBar_SetText($StatusBar1, "", 2)

TraySetIcon("F:\Program Files\ST-Link-Utility\ST-LINK Utility\ST-LinkUpgrade.exe", -1)
TraySetClick("0")
$MenuItem2 = TrayCreateMenu("Show")
$MenuItem3 = TrayCreateItem("MenuItem3", $MenuItem2)
$MenuItem1 = TrayCreateItem("Hide")
GUISetState(@SW_SHOW)
#endregion GUI


;GUICtrlSetOnEvent($startstop, "_recv")
GUICtrlSetOnEvent($GUI_EVENT_CLOSE, "_Exit")




$portopenErr = ""
$connectbutton_ptr = 0x00

$recv_running = 0
$connected = "Closed"
$i = 0

;ConsoleWrite("Available Ports: " & $portlist & @CRLF);retreive and print available ports





While 1
	$nMsg = GUIGetMsg()
	Switch $nMsg
		Case $GUI_EVENT_CLOSE
			Exit
		Case $refreshlist

			GUICtrlSetData($portbox, "")
			$portlist = _CommListPorts()
			GUICtrlSetData($portbox, $portlist)
		Case $Button2

			_CommClearInputBuffer()

		Case $singlestep
			If GUICtrlRead($Checkbox1) = $GUI_CHECKED Then
				_CommClearInputBuffer()
			EndIf
			ConsoleWrite(StringRegExpReplace(StringTrimLeft(StringToBinary(_CommGetLine("b", 48, 0)), 2), "(..)", "\1 ") & @CRLF)
		Case $Button4

			$test86 = _Commgetstring()
			ConsoleWrite(StringRegExpReplace(StringTrimLeft(StringToBinary($test86), 2), "(..)", "\1 ") & @CRLF)
		Case $connectbutton

			If GUICtrlRead($connectbutton) = "Connect" Then ;check whether to con or dc
				GUICtrlSetData($connectbutton, "Disconnect")

				$baudr = GUICtrlRead($baudinput) ;read in baudr from inputbox
				$comport = Number(StringRight(GUICtrlRead($portbox), 1)) ;read port from checkbox (stringright to filter last character, number to convert) !!! needs update for ports higher than 9
				If IsNumber($comport) And $comport > 0 Then
					If _CommSetPort($comport, $portopenErr, $baudr, 8, 0, 1, 0, 0, 0) Then
						$connected = "Established"
						ConsoleWrite("port " & $comport & " opened at " & $baudr & " baud" & @CRLF)
					Else
						MsgBox(0, "Error", "Could not open Port" & @CRLF & "Check Log")
					EndIf
				Else
					MsgBox(0, "Error", "Could not open Port" & @CRLF & "No Port selected or higher than 9?") ;add support for manual port number input
				EndIf
			Else
				GUICtrlSetData($connectbutton, "Connect")
				If Not _CommClosePort() Then
					$connected = "Closed"
					ConsoleWrite("port " & $comport & " closed." & @CRLF)
				Else
					MsgBox(0, "Error", "Could not close Port")
				EndIf
			EndIf
		Case $startstop

			_recv2()


	EndSwitch

	_GUICtrlStatusBar_SetText($StatusBar1, "Connection Status: " & $connected, 2)

	If $connected = "Established" Then
		GUICtrlSetState($startstop, $GUI_ENABLE)
		GUICtrlSetState($singlestep, $GUI_ENABLE)
	Else
		GUICtrlSetState($startstop, $GUI_DISABLE)
		GUICtrlSetState($singlestep, $GUI_DISABLE)
	EndIf

	_GUICtrlStatusBar_SetText($StatusBar1, "Available Packets: " & _CommGetInputCount(), 1)

	;Sleep(10)
WEnd








;StringRegExpReplace(StringTrimLeft(StringToBinary($String), 2),"(..)","\1 ")




;if  not (_CommSetPort($comport, $portopenErr, $baudr, 8, 0, 1, 0, 0, 0)) Then		;open comport with desired settings		(data, parity, stop, flow, rts, dtr)
;	  ConsoleWrite("error opening port: " & $portopenErr)
;  Else
;	  ConsoleWrite("port " & $comport & " opened at " & $baudr & " baud" & @CRLF)
;   EndIf

;$null = 0
;$nullstring = ""

Global $rec_byte_array[50]

;$rec_byte = _CommReadChar(1) ;listen until byte is received
;$rec_line = _CommGetLine("�", 50, 0) ;listen until "�" or 50bytes are received
;$rec_line = _CommGetLine("b", 77, 0)

;FileOpen("huehue.txt")
;FileWrite("huehue.txt", $rec_line)
;FileClose("huehue.txt")

;For $i = 0 To 48
;	$rec_byte_array[$i] = _CommReadByte(1)
;Next

;ConsoleWrite("data: " & @CRLF)

;For $i = 0 To 48
;	ConsoleWrite($i & ": " & $rec_byte_array[$i] & @CRLF)
;Next




;ConsoleWrite("0: " & $null & @CRLF)
;ConsoleWrite("0 hex: " & Hex($null) & @CRLF)
;ConsoleWrite("0 bin: " & Binary($null) & @CRLF)

;ConsoleWrite("string 0: " & $nullstring & @CRLF)
;ConsoleWrite("string 0 hex: " & Hex($nullstring) & @CRLF)
;ConsoleWrite("string 0 bin: " & Binary($nullstring) & @CRLF)
;ConsoleWrite("string 0 stb: " & StringToBinary($nullstring) & @CRLF)
;ConsoleWrite("string 0 stb+hex: " & Hex(StringToBinary($nullstring)) & @CRLF)
;ConsoleWrite("string 0 stb+bin: " & Binary(StringToBinary($nullstring)) & @CRLF)

;ConsoleWrite("roh byte: " & $rec_byte & @CRLF)
;ConsoleWrite("roh line: " & $rec_line & @CRLF)
;ConsoleWrite("roh line hex: " & Hex($rec_line) & @CRLF)
;ConsoleWrite("roh byte bin: " & Binary($rec_byte) & @CRLF)
;ConsoleWrite(Hex(StringToBinary($rec_line)) & @CRLF)
;ConsoleWrite(Binary(StringToBinary($rec_line)) & @CRLF)
;MsgBox(0, "", $rec_byte)
;MsgBox(0, "", Hex($rec_byte, 2))
;MsgBox(0, "new", StringRegExpReplace(StringTrimLeft(StringToBinary($rec_byte), 4),"(..)","\1 "))

;MsgBox(0, "getline", _CommGetLine(255, 50, 0)) ;return strings until 0xFF is received
;MsgBox(0, "new", StringToBinary($rec_line))
;ConsoleWrite(_CommSetPort($comport, $portopenErr, $baudr, 8, 0, 1, 0, 0, 0) & @CRLF)



Func _recv()

	While GUIGetMsg() <> $startstop
		;		$nMsg = GUIGetMsg()
		;		If $nMsg = $startstop Then
		;			ConsoleWrite("stop" & @CRLF)
		;			ExitLoop
		;		EndIf
		If $i = 48 Then
			ConsoleWrite(@CRLF)
			$i = 0
		EndIf
		;$rec_byte = _CommReadByte(1)
		ConsoleWrite(_CommReadByte(1) & "   ")
		;Sleep(10)
		$i += 1

		_GUICtrlStatusBar_SetText($StatusBar1, "Available Packets: " & _CommGetInputCount(), 1)
	WEnd
EndFunc   ;==>_recv

Func _recv2()

	While GUIGetMsg() <> $startstop

		;$567567 = _CommGetLine("�", 50, 0)
		ConsoleWrite(StringRegExpReplace(StringTrimLeft(StringToBinary(_CommGetLine("bbbb", 48, 0)), 2), "(..)", "\1 ") & @CRLF)

		_GUICtrlStatusBar_SetText($StatusBar1, "Available Packets: " & _CommGetInputCount(), 1)
	WEnd
EndFunc   ;==>_recv2



Func _Exit()
	Exit
EndFunc   ;==>_Exit

