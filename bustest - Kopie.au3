#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Change2CUI=y
#AutoIt3Wrapper_Run_Au3Stripper=y
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
;#AutoIt3Wrapper_Res_File_Add=test_1.txt, rt_rcdata, TEST_TXT_1




;#include <MsgBoxConstants.au3>
#include <Constants.au3>
#include <ButtonConstants.au3>
#include <EditConstants.au3>
#include <GUIConstantsEx.au3>
#include <GUIListBox.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>
#include <GuiStatusBar.au3>
#include <Array.au3>
#include "resources.au3"
#include "CommMG.au3"
#include <EditConstants.au3>
#Include <GUIEdit.au3>
#Include <ScrollBarConstants.au3>
;#include <TrayConstants.au3>

Opt("TrayMenuMode", 1)
;Opt("TrayMenuMode", 0)
;Opt("TrayMenuMode", 0)
;Opt("GUIOnEventMode", 1)

#region GUI

$Form1 = GUICreate("Bus", 613, 438, 192, 124)
$portbox = GUICtrlCreateList("", 40, 48, 81, 84)
$Button2 = GUICtrlCreateButton("Clear I/Buffer", 280, 40, 75, 25)
$singlestep = GUICtrlCreateButton("Single", 280, 120, 75, 25)
$Button4 = GUICtrlCreateButton("test345", 280, 185, 75, 25)
$testbutton = GUICtrlCreateButton("Test", 280, 250, 75, 25)
$baudinput = GUICtrlCreateInput("250000", 168, 56, 73, 21, BitOR($GUI_SS_DEFAULT_INPUT, $ES_RIGHT, $ES_NUMBER))
	GUICtrlSetBkColor(-1, 0xBFCDDB)
$connectbutton = GUICtrlCreateButton("Connect", 168, 104, 75, 25, $BS_DEFPUSHBUTTON)
$startstop = GUICtrlCreateButton("Start/Stop", 280, 80, 75, 25, $GUI_DISABLE)
GUIStartGroup()
$Checkbox1 = GUICtrlCreateCheckbox("Clear on read", 40, 200, 100, 20)
$Checkbox2 = GUICtrlCreateCheckbox("Write output to file", 40, 224, 100, 20, $BS_3STATE, $BS_3STATE)
$Checkbox3 = GUICtrlCreateCheckbox("Write Log", 40, 248, 100, 20, $BS_3STATE, $BS_3STATE)
	GUICtrlSetState($Checkbox1, $GUI_CHECKED)
$bin = GUICtrlCreateRadio("BIN", 176, 200, 100, 17)
$hex = GUICtrlCreateRadio("HEX", 176, 224, 100, 17)
$dec = GUICtrlCreateRadio("DEC", 176, 248, 100, 17)
	GUICtrlSetState($hex, $GUI_CHECKED)

$CMD_WINDOW = GUICtrlCreateEdit("", 10, 300, 600, 89, BitOR($ES_AUTOVSCROLL,$ES_AUTOHSCROLL,$ES_READONLY,$WS_VSCROLL))
    GUICtrlSetFont($CMD_WINDOW, 12, 800, 0, "Times New Roman")
    GUICtrlSetColor($CMD_WINDOW, 0xFFFFFF)
    GUICtrlSetBkColor($CMD_WINDOW, 0x000000)
	GUICtrlSetCursor($CMD_WINDOW, 5)


GUIStartGroup()
$Group1 = GUICtrlCreateGroup(" Output Settings ", 24, 176, 241, 105)
GUICtrlCreateGroup("", -99, -99, 1, 1)
$Group2 = GUICtrlCreateGroup(" Main Settings ", 24, 24, 241, 129)
$Label1 = GUICtrlCreateLabel("Baud", 128, 56, 32, 18)
GUICtrlSetFont(-1, 11, 400, 0, "MS Sans Serif")
GUICtrlSetResizing(-1, $GUI_DOCKAUTO)
$refreshlist = GUICtrlCreateButton("rf", 128, 80, 27, 25, $BS_BITMAP)
GUICtrlSetImage(-1, "C:\Users\JannisDesktop\Desktop\RETRY.BMP", -1)
GUICtrlCreateGroup("", -99, -99, 1, 1)

$fileoptions = GUICtrlCreateGroup("File Options", 24, 288, 113, 65)
$totxt = GUICtrlCreateCheckbox("Export to .txt", 40, 304, 89, 17)
$tocsv = GUICtrlCreateCheckbox("Export to Excel", 40, 328, 89, 17)
GUICtrlCreateGroup("", -99, -99, 1, 1)
GUICtrlSetState($tocsv, $GUI_HIDE)
GUICtrlSetState($totxt, $GUI_HIDE)
GUICtrlSetState($fileoptions, $GUI_HIDE)

$Pic1 = GUICtrlCreatePic("C:\Users\JannisDesktop\Desktop\Unbenannt.JPG", 376, 24, 217, 137, BitOR($GUI_SS_DEFAULT_PIC, $GUI_AVISTOP))		;, $WS_BORDER

$StatusBar1 = _GUICtrlStatusBar_Create($Form1)
Dim $StatusBar1_PartsWidth[3] = [200, 400, -1]
_GUICtrlStatusBar_SetParts($StatusBar1, $StatusBar1_PartsWidth)
_GUICtrlStatusBar_SetText($StatusBar1, "", 0)
_GUICtrlStatusBar_SetText($StatusBar1, "", 1)
_GUICtrlStatusBar_SetText($StatusBar1, "", 2)

TraySetIcon("F:\Program Files\ST-Link-Utility\ST-LINK Utility\ST-LinkUpgrade.exe", -1)
;TraySetClick("0")
$MenuItem2 = TrayCreateMenu("Show")
$MenuItem3 = TrayCreateItem("MenuItem3", $MenuItem2)
$MenuItem1 = TrayCreateItem("Hide")
TraySetState(True)


GUISetState(@SW_SHOW)

#endregion GUI


;GUICtrlSetOnEvent($startstop, "_recv")
GUICtrlSetOnEvent($GUI_EVENT_CLOSE, "_Exit")

$portopenErr = ""
$connectbutton_ptr = 0x00

$connected = "Closed"
$i = 0

;ConsoleWrite("Available Ports: " & $portlist & @CRLF);retreive and print available ports

Global $tim_rec_int = TimerInit()			;init receiving delay timer
Global $tim_2 = TimerInit()
Global $tim_main = TimerInit()
$rec_delay = 100							;delay in ms

Global Const $teststring = "02 FA FA FA FA FA FA FA 08 01 00 00 00 00 00 00 00 00 00 01 01 01 01 01 01 01 01 01 01 01 01 01 01 99 FF 00 01 01 00 00 00 00 32 09 01 CB 00 20 02 F9 F9 F9 F9 F9 F9 F9 08 08 00 00 00 00 00 00 00 00 00 01 01 01 01 01 01 01 01 01 01 01 01 01 01 01 FF 00 01 01 00 00 00 00 32 09 01 CB 00 20"

Global $record
Global $running_flag = 0



GUICtrlSetData($portbox, "")
$portlist = _CommListPorts()
GUICtrlSetData($portbox, $portlist)

While 1
	$nMsg = GUIGetMsg()
	Switch $nMsg
		Case $GUI_EVENT_CLOSE						;x clicked
			Exit
		Case $refreshlist							;button clicked

			GUICtrlSetData($portbox, "")
			$portlist = _CommListPorts()
			GUICtrlSetData($portbox, $portlist)

		Case $Button2							;button clicked

			_CommClearInputBuffer()
			;_toggle_fileoptions()
		Case $singlestep							;button clicked

			If IsChecked($Checkbox1) Then
				_CommClearInputBuffer()
			EndIf
			ConsoleWrite(StringRegExpReplace(StringTrimLeft(StringToBinary(_CommGetLine(333, 48, 0)), 2), "(..)", "\1 ") & @CRLF)

		Case $Button4							;button clicked

			;$test86 = _Commgetstring()
			;ConsoleWrite(StringRegExpReplace(StringTrimLeft(StringToBinary($test86), 2), "(..)", "\1 ") & @CRLF)
			$iEnd = StringLen(GUICtrlRead($CMD_WINDOW))
            _GUICtrlEdit_SetSel($CMD_WINDOW, $iEnd, $iEnd)
            _GUICtrlEdit_Scroll($CMD_WINDOW, $SB_SCROLLCARET)
			GUICtrlSetData($CMD_WINDOW, @CRLF & "huehue", 1)		; ,1 um hinzuzuf�gen

		Case $testbutton							;button clicked

			_recv4()

		Case $connectbutton							;button clicked

			If GUICtrlRead($connectbutton) = "Connect" Then ;check whether to con or dc

				_connect2port()

			Else

				_closeport()

			EndIf

		Case $startstop							;button clicked

			$running_flag = BitXOR($running_flag, True)


	EndSwitch

	If(TimerDiff($tim_main) >= 500) Then

		_GUICtrlStatusBar_SetText($StatusBar1, "null", 0)
		_GUICtrlStatusBar_SetText($StatusBar1, "Available Packets: " & _CommGetInputCount(), 1)
		_GUICtrlStatusBar_SetText($StatusBar1, "Connection Status: " & $connected, 2)

;************ Dis/Enable buttons *********************
		If $connected = "Established" Then
			GUICtrlSetState($startstop, $GUI_ENABLE)
			GUICtrlSetState($singlestep, $GUI_ENABLE)
		Else
			GUICtrlSetState($startstop, $GUI_DISABLE)
			GUICtrlSetState($singlestep, $GUI_DISABLE)
		EndIf
;******************************************************					BUGGED
		If(IsChecked($Checkbox2)) Then
			_toggle_fileoptions()
		EndIf
;*****************************************************					BUGGED
	EndIf


	If (TimerDiff($tim_rec_int) >= $rec_delay	And	   $running_flag) Then
		$tim_rec_int = TimerInit()
		$record = _recv3(1)
		;_ArrayDisplay($record)
		ConsoleWrite($record & @CRLF)
		_PrintFromArray($record)
		$i+=1
		ConsoleWrite($i & @CRLF)
	Endif
	;Sleep(10)
WEnd






#region old

;StringRegExpReplace(StringTrimLeft(StringToBinary($String), 2),"(..)","\1 ")




;if  not (_CommSetPort($comport, $portopenErr, $baudr, 8, 0, 1, 0, 0, 0)) Then		;open comport with desired settings		(data, parity, stop, flow, rts, dtr)
;	  ConsoleWrite("error opening port: " & $portopenErr)
;  Else
;	  ConsoleWrite("port " & $comport & " opened at " & $baudr & " baud" & @CRLF)
;   EndIf

;$null = 0
;$nullstring = ""

;Global $rec_byte_array[50]

;$rec_byte = _CommReadChar(1) ;listen until byte is received
;$rec_line = _CommGetLine("�", 50, 0) ;listen until "�" or 50bytes are received
;$rec_line = _CommGetLine("b", 77, 0)

;FileOpen("huehue.txt")
;FileWrite("huehue.txt", $rec_line)
;FileClose("huehue.txt")

;For $i = 0 To 48
;	$rec_byte_array[$i] = _CommReadByte(1)
;Next

;ConsoleWrite("data: " & @CRLF)

;For $i = 0 To 48
;	ConsoleWrite($i & ": " & $rec_byte_array[$i] & @CRLF)
;Next




;ConsoleWrite("0: " & $null & @CRLF)
;ConsoleWrite("0 hex: " & Hex($null) & @CRLF)
;ConsoleWrite("0 bin: " & Binary($null) & @CRLF)

;ConsoleWrite("string 0: " & $nullstring & @CRLF)
;ConsoleWrite("string 0 hex: " & Hex($nullstring) & @CRLF)
;ConsoleWrite("string 0 bin: " & Binary($nullstring) & @CRLF)
;ConsoleWrite("string 0 stb: " & StringToBinary($nullstring) & @CRLF)
;ConsoleWrite("string 0 stb+hex: " & Hex(StringToBinary($nullstring)) & @CRLF)
;ConsoleWrite("string 0 stb+bin: " & Binary(StringToBinary($nullstring)) & @CRLF)

;ConsoleWrite("roh byte: " & $rec_byte & @CRLF)
;ConsoleWrite("roh line: " & $rec_line & @CRLF)
;ConsoleWrite("roh line hex: " & Hex($rec_line) & @CRLF)
;ConsoleWrite("roh byte bin: " & Binary($rec_byte) & @CRLF)
;ConsoleWrite(Hex(StringToBinary($rec_line)) & @CRLF)
;ConsoleWrite(Binary(StringToBinary($rec_line)) & @CRLF)
;MsgBox(0, "", $rec_byte)
;MsgBox(0, "", Hex($rec_byte, 2))
;MsgBox(0, "new", StringRegExpReplace(StringTrimLeft(StringToBinary($rec_byte), 4),"(..)","\1 "))

;MsgBox(0, "getline", _CommGetLine(255, 50, 0)) ;return strings until 0xFF is received
;MsgBox(0, "new", StringToBinary($rec_line))
;ConsoleWrite(_CommSetPort($comport, $portopenErr, $baudr, 8, 0, 1, 0, 0, 0) & @CRLF)
#endregion


Func _recv()

	While GUIGetMsg() <> $startstop
		;		$nMsg = GUIGetMsg()
		;		If $nMsg = $startstop Then
		;			ConsoleWrite("stop" & @CRLF)
		;			ExitLoop
		;		EndIf
		If $i = 48 Then
			ConsoleWrite(@CRLF)
			$i = 0
		EndIf
		;$rec_byte = _CommReadByte(1)
		ConsoleWrite(_CommReadByte(1) & "   ")
		;Sleep(10)
		$i += 1

		_GUICtrlStatusBar_SetText($StatusBar1, "Available Packets: " & _CommGetInputCount(), 1)
	WEnd
EndFunc   ;==>_recv

Func _recv2()

	While GUIGetMsg() <> $startstop

		;$567567 = _CommGetLine("�", 50, 0)
		ConsoleWrite(StringRegExpReplace(StringTrimLeft(StringToBinary(_CommGetLine("bbbb", 48, 0)), 2), "(..)", "\1 ") & @CRLF)

		_GUICtrlStatusBar_SetText($StatusBar1, "Available Packets: " & _CommGetInputCount(), 1)
	WEnd
EndFunc   ;==>_recv2


Func _recv3(Const $records = 1)
	;ConsoleWrite("Debug: " & "records= " & $records & @CRLF)
	Local $temp_in[$records]
	Local $readings[$records][48]

	If IsChecked($Checkbox1) Then
		_CommClearInputBuffer()
	;	ConsoleWriteError("sleep" & @CRLF)
		Sleep(9*$records)
	EndIf
;	ConsoleWriteError("get values" & @CRLF)

	For $i = 0 To $records-1
		$temp_in[$i] = StringRegExpReplace(StringTrimLeft(StringToBinary(_CommGetLine(333, 48, 0)), 2), "(..)", "\1 ")
		;$temp_in[$i] = "02 FA FA FA FA FA FA FA 08 01 00 00 00 00 00 00 00 00 00 01 01 01 01 01 01 01 01 01 01 01 01 01 01 01 FF 00 01 01 00 00 00 00 32 09 01 CB 00 20"

		;$i2 = 0

		If(StringLen($temp_in[$i]) < 48) Then
			MsgBox(0, "Error", "Received String(s) were empty or too short after 25 retrys" & @CRLF & $temp_in[$i])
			Return SetError(1, 0, 0)
		EndIf

;		While(StringLen($temp_in[$i]) < 48)
;
;			If $i2 = 25 Then
;
;				MsgBox(0, "Error", "Received String(s) were empty or too short after 25 retrys" & @CRLF & $temp_in[$i])
;				Return SetError(1, 0, 0)
;			EndIf
;
;			$i2 += 1
;
;			$temp_in[$i] = StringRegExpReplace(StringTrimLeft(StringToBinary(_CommGetLine(@CR, 48, 0)), 2), "(..)", "\1 ")
;			;@error=1
;			;MsgBox(0, "error", "Received String was empty or too short" & @CRLF & $temp_in[$i])
;			;SetError(1)
;			;Return
;		;EndIf
;		WEnd

	Next
;	ConsoleWriteError("convert" & @CRLF)

	;_ArrayDisplay($readings)
	For $i = 0 To $records-1

		$temparray = StringSplit($temp_in[$i], " ")

;	ConsoleWriteError("recheck" & @CRLF)
		If Ubound($temparray) > 49 Then
;			Sleep(1000)
			_ArrayDelete($temparray, 49)
		;ElseIf Ubound($temparray) < 48 Then

		;	MsgBox(0, "Error", "In conversion of record " & $i & " of " & $records & "; array too short" & @CRLF & $temp_in[$i])
		;	_ArrayDisplay($temparray)
		;	Return SetError(2, 0, 0)

		EndIf

;		ConsoleWriteError("sort" & @CRLF)

		;_ArrayDisplay($temparray)
		$temparray = _sortarray($temparray)

		If @error Then
			$running_flag = BitXOR($running_flag, True)
			ToolTip( @CRLF & "    Stopped    " & @CRLF & " ", 200, 150)
			Sleep(1000)
			ToolTip("")
		EndIf
		;ConsoleWrite(UBound($temparray))


;		ConsoleWriteError("allocate" & @CRLF)

		For $i2 = 0 To (UBound($temparray))-1
			$readings[$i][$i2]=$temparray[($i2)]
		Next
	Next

;	ConsoleWriteError("done" & @CRLF)

	;$readings=Hex($readings)
	;_ArrayDisplay($readings)

	Return ($readings)
EndFunc    ;==>_recv3


Func _recv4(Const $records = 1)
	;ConsoleWrite("Debug: " & "records= " & $records & @CRLF)
	Local $temp_in[96]
	Local $temp_in_double[$records]
	Local $readings[2][48]				;formerly $records;
	Local $temparray[48]

	If IsChecked($Checkbox1) Then
		_CommClearInputBuffer()
	;	ConsoleWriteError("sleep" & @CRLF)
		Sleep(9*$records)
	EndIf
;	ConsoleWriteError("get values" & @CRLF)

	For $i = 0 To $records-1
		;;;;;;;;;;;;;$temp_in[$i] = StringRegExpReplace(StringTrimLeft(StringToBinary(_CommGetLine(333, 96, 0)), 2), "(..)", "\1 ")
		;^^
		$temp_in_double[$i] = $teststring
		;$temp_in[$i] = "02 FA FA FA FA FA FA FA 08 01 00 00 00 00 00 00 00 00 00 01 01 01 01 01 01 01 01 01 01 01 01 01 01 01 FF 00 01 01 00 00 00 00 32 09 01 CB 00 20"

		;$i2 = 0

		If(StringLen($temp_in_double[$i]) < 96) Then
			MsgBox(0, "Error", "Received String(s) were empty or too short after 25 retrys" & @CRLF & $temp_in_double[$i])
			Return SetError(1, 0, 0)
		EndIf

;		While(StringLen($temp_in[$i]) < 48)
;
;			If $i2 = 25 Then
;
;				MsgBox(0, "Error", "Received String(s) were empty or too short after 25 retrys" & @CRLF & $temp_in[$i])
;				Return SetError(1, 0, 0)
;			EndIf
;
;			$i2 += 1
;
;			$temp_in[$i] = StringRegExpReplace(StringTrimLeft(StringToBinary(_CommGetLine(@CR, 48, 0)), 2), "(..)", "\1 ")
;			;@error=1
;			;MsgBox(0, "error", "Received String was empty or too short" & @CRLF & $temp_in[$i])
;			;SetError(1)
;			;Return
;		;EndIf
;		WEnd

	Next
;	ConsoleWriteError("convert" & @CRLF)
	$temp_in = StringSplit($temp_in_double[0], " ", 2)

	_ArrayDisplay($temp_in)
	;_ArrayDisplay($readings)
	$tmp = Ubound($temp_in, 1)-49
	For $i = 0 To $records						;-----------------ONLY WORKS WITH 1 RECORD ATM

		_ArrayDisplay($temp_in, "temp_in, " & $i)

		For $i3 = Ubound($temp_in,1)-1 To $tmp+1 Step -1
			ConsoleWrite("storing: " & $i3-48 & @CRLF & $tmp+1 & @CRLF)
			$temparray[$i3-48*($i+1)] = _ArrayPop($temp_in)
			;_PrintFromArray($temp_in)
		Next
		_ArrayDisplay($temparray, "temparray, " & $i)
;	ConsoleWriteError("recheck" & @CRLF)
		If Ubound($temparray) > 49 Then
;			Sleep(1000)
			_ArrayDelete($temparray, 49)
		;ElseIf Ubound($temparray) < 48 Then

		;	MsgBox(0, "Error", "In conversion of record " & $i & " of " & $records & "; array too short" & @CRLF & $temp_in[$i])
		;	_ArrayDisplay($temparray)
		;	Return SetError(2, 0, 0)

		EndIf

;		ConsoleWriteError("sort" & @CRLF)

		;_ArrayDisplay($temparray)
		$temparray = _sortarray($temparray)

		If @error Then
			$running_flag = False
			ToolTip( @CRLF & "    Stopped    " & @CRLF & " ", 200, 222)
			Sleep(1500)
			ToolTip("")
		EndIf
		;ConsoleWrite(UBound($temparray))


;		ConsoleWriteError("allocate" & @CRLF)

		For $i2 = 0 To (UBound($temparray))-1
			ConsoleWrite("convert: " & $i & @CRLF)
			$readings[$i][$i2]=$temparray[($i2)]
		Next
		_ArrayDisplay($readings, "END")
	Next

;	ConsoleWriteError("done" & @CRLF)

	;$readings=Hex($readings)
	;_ArrayDisplay($readings)

	Return ($readings)
EndFunc    ;==>_recv4


Func _Exit()

	Exit
EndFunc   ;==>_Exit


Func _connect2port()

	GUICtrlSetData($connectbutton, "Disconnect")
	$baudr = GUICtrlRead($baudinput) ;read in baudr from inputbox
	Global $comport = Number(StringRight(GUICtrlRead($portbox), 1)) ;read port from checkbox (stringright to filter last character, number to convert) !!! needs update for ports higher than 9

	If IsNumber($comport) And $comport > 0 Then
		If _CommSetPort($comport, $portopenErr, $baudr, 8, 0, 1, 0, 0, 0) Then
			$connected = "Established"
			ConsoleWrite("port " & $comport & " opened at " & $baudr & " baud" & @CRLF)
		Else
			MsgBox(0, "Error", "Could not open Port" & @CRLF & "")
			_closeport()
		EndIf
	Else
		MsgBox(0, "Error", "Could not open Port" & @CRLF & "No Port selected or higher than 9?") ;add support for manual port number input
		_closeport()
	EndIf
EndFunc    ;==>_connect2port


Func _closeport()

	GUICtrlSetData($connectbutton, "Connect")
	If Not _CommClosePort() Then
		$connected = "Closed"
		ConsoleWrite("port " & $comport & " closed." & @CRLF)
	Else
		MsgBox(0, "Error", "Could not close Port")
	EndIf
EndFunc    ;==>_closeport


Func IsChecked (Const ByRef $controlID)

    If (BitAND (GUICtrlRead ($controlID), $GUI_CHECKED)) Then _
        Return TRUE

    Return FALSE
EndFunc


Func IsEnabled (Const ByRef $controlID)

    If (BitAND (GUICtrlGetState ($controlID), $GUI_ENABLE)) Then _
        Return TRUE

    Return FALSE
EndFunc

Func _toggle_fileoptions()
If (BitAND(GUICtrlGetState($fileoptions), $GUI_HIDE)) Then
	GUICtrlSetState($fileoptions, $GUI_SHOW)
	GUICtrlSetState($tocsv, $GUI_SHOW)
	GUICtrlSetState($totxt, $GUI_SHOW)
Else
	GUICtrlSetState($fileoptions, $GUI_HIDE)
	GUICtrlSetState($tocsv, $GUI_HIDE)
	GUICtrlSetState($totxt, $GUI_HIDE)
EndIf

;$fileoptions = BitXOR($fileoptions, $GUI_SHOW, $GUI_HIDE)
;ConsoleWrite("t2 " & $fileoptions)
;GUICtrlSetState($fileoptions,BitXOR($fileoptions, $GUI_SHOW, $GUI_HIDE))
;GUICtrlSetState($tocsv,BitXOR($tocsv, $GUI_SHOW, $GUI_HIDE))
;GUICtrlSetState($totxt,BitXOR($totxt, $GUI_SHOW, $GUI_HIDE))
EndFunc    ;==>_toggle_fileoptions


Func _sortarray($array)

	Local $limiter = 0

	If(UBound($array) <> 48) Then
		MsgBox(0, "Error", "In _sortarray; array too short" & @CRLF)
		$running_flag = 0
		_ArrayDisplay($array)
		Return SetError(1, 0, 0)
	EndIf

	;_ArrayDelete($array, 0)			former removal of first element, containing arraysize

	While($array[0] <> "FF")

		If($limiter > 49) Then
			MsgBox(0, "Error", "In _sortarray; no FF found" & @CRLF)
			_ArrayDisplay($array)
			Return SetError(1, 0, 0)
		EndIf

		_ArrayPush($array, $array[47], 1)
		$limiter += 1
	WEnd
	Return($array)
EndFunc	   ;==>_sortarray


; #FUNCTION# ====================================================================================================================
; Name ..........: _PrintFromArray
; Description ...: Print an array to the console.
; Syntax ........: _PrintFromArray(Const Byref $aArray[, $iBase = Default[, $iUBound = Default[, $sDelimeter = "|"]]])
; Parameters ....: $aArray              - [in/out and const] The array to be written to the file.
;                  $iBase               - [optional] Start array index to read, normally set to 0 or 1. Default is 0.
;                  $iUBound             - [optional] Set to the last record you want to write to the File. Default is whole array.
;                  $sDelimeter          - [optional] Delimiter character(s) for 2-dimension arrays. Default is "|".
; Return values .: Success - 1
;                  Failure - 0 and sets @error to non-zero
;                   |@error:
;                   |1 - Input is not an array.
;                   |2 - Array dimension is greater than 2.
;                   |3 - Start index is greater than the size of the array.
; Author ........: guinness
; Modified ......:
; Remarks .......:
; Related .......: _FileWriteFromArray
; Link ..........:
; Example .......: Yes
; ===============================================================================================================================
Func _PrintFromArray(ByRef Const $aArray, $iBase = Default, $iUBound = Default, $sDelimeter = "|")
    ; Check if we have a valid array as input
    If Not IsArray($aArray) Then Return SetError(1, 0, 0)

    ; Check the number of dimensions
    Local $iDims = UBound($aArray, 0)
    If $iDims > 2 Then Return SetError(2, 0, 0)

    ; Determine last entry of the array
    Local $iLast = UBound($aArray) - 1
    If $iUBound = Default Or $iUBound > $iLast Then $iUBound = $iLast
    If $iBase < 0 Or $iBase = Default Then $iBase = 0
    If $iBase > $iUBound Then Return SetError(3, 0, 0)

    If $sDelimeter = Default Then $sDelimeter = "|"

    ; Write array data to the console
    Switch $iDims
        Case 1
            For $i = $iBase To $iUBound
                ConsoleWrite("[" & $i - $iBase & "] " & $aArray[$i] & @CRLF)
            Next
        Case 2
            Local $sTemp = ""
            Local $iCols = UBound($aArray, 2)
            For $i = $iBase To $iUBound
                $sTemp = $aArray[$i][0]
                For $j = 1 To $iCols - 1
                    $sTemp &= $sDelimeter & $aArray[$i][$j]
                Next
                ConsoleWrite("[" & $i - $iBase & "] " & $sTemp & @CRLF)
            Next
    EndSwitch
    Return 1
EndFunc   ;==>_PrintFromArray


Func _ArrayCombine(ByRef $array1,ByRef $array2) ; Function to combine 2D arrays

	Local $diff = (Abs(UBound($array1, 2)-UBound($array2, 2)))
	Local $a1_c = UBound($array1, 2)
	Local $a1_r = Ubound($array1,1)+UBound($array2, 1)
	Local $a1_r_old = Ubound($array1,1)
	;ConsoleWrite(@CRLF & "test: " & $diff & " , " & $u1 & " , " & $u2 & @CRLF)
	If(UBound($array2, 2)>UBound($array1, 2)) Then $a1_c = UBound($array1,2)+$diff
	ConsoleWrite(Ubound($array2,1)-1 & @CRLF)

    For $i=0 To Ubound($array2,1)-1
		ReDim $array1[$a1_r][$a1_c]
		;ConsoleWrite(UBound($array1) & @CRLF)
		For $i_2 = 0 To $a1_c-1
			;ConsoleWrite($i_2 & @CRLF)
			$array1[$i+$a1_r_old][$i_2]=$array2[$i][$i_2]
		Next

        ;$array1[Ubound($array1,1)-1][1]=$array2[$i][1]
    Next
EndFunc
