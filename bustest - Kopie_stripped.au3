Global Const $BS_DEFPUSHBUTTON = 0x0001
Global Const $BS_BITMAP = 0x0080
Global Const $ES_LEFT = 0
Global Const $ES_RIGHT = 2
Global Const $ES_AUTOHSCROLL = 128
Global Const $ES_NUMBER = 8192
Global Const $GUI_SS_DEFAULT_INPUT = BitOR($ES_LEFT, $ES_AUTOHSCROLL)
Global Const $GUI_EVENT_CLOSE = -3
Global Const $GUI_AVISTOP = 0
Global Const $GUI_CHECKED = 1
Global Const $GUI_SHOW = 16
Global Const $GUI_HIDE = 32
Global Const $GUI_ENABLE = 64
Global Const $GUI_DISABLE = 128
Global Const $GUI_DOCKAUTO = 0x0001
Global Const $MEM_COMMIT = 0x00001000
Global Const $MEM_RESERVE = 0x00002000
Global Const $PAGE_READWRITE = 0x00000004
Global Const $MEM_RELEASE = 0x00008000
Global Const $tagGDIPSTARTUPINPUT = "uint Version;ptr Callback;bool NoThread;bool NoCodecs"
Global Const $tagTOKEN_PRIVILEGES = "dword Count;align 4;int64 LUID;dword Attributes"
Global Const $PROCESS_VM_OPERATION = 0x00000008
Global Const $PROCESS_VM_READ = 0x00000010
Global Const $PROCESS_VM_WRITE = 0x00000020
Global Const $ERROR_NO_TOKEN = 1008
Global Const $SE_PRIVILEGE_ENABLED = 0x00000002
Global Enum $SECURITYANONYMOUS = 0, $SECURITYIDENTIFICATION, $SECURITYIMPERSONATION, $SECURITYDELEGATION
Global Const $TOKEN_QUERY = 0x00000008
Global Const $TOKEN_ADJUST_PRIVILEGES = 0x00000020
Func _WinAPI_GetLastError($curErr = @error, $curExt = @extended)
Local $aResult = DllCall("kernel32.dll", "dword", "GetLastError")
Return SetError($curErr, $curExt, $aResult[0])
EndFunc
Func _SendMessage($hWnd, $iMsg, $wParam = 0, $lParam = 0, $iReturn = 0, $wParamType = "wparam", $lParamType = "lparam", $sReturnType = "lresult")
Local $aResult = DllCall("user32.dll", $sReturnType, "SendMessageW", "hwnd", $hWnd, "uint", $iMsg, $wParamType, $wParam, $lParamType, $lParam)
If @error Then Return SetError(@error, @extended, "")
If $iReturn >= 0 And $iReturn <= 4 Then Return $aResult[$iReturn]
Return $aResult
EndFunc
Global $__gaInProcess_WinAPI[64][2] = [[0, 0]]
Global Const $HGDI_ERROR = Ptr(-1)
Global Const $INVALID_HANDLE_VALUE = Ptr(-1)
Global Const $KF_EXTENDED = 0x0100
Global Const $KF_ALTDOWN = 0x2000
Global Const $KF_UP = 0x8000
Global Const $LLKHF_EXTENDED = BitShift($KF_EXTENDED, 8)
Global Const $LLKHF_ALTDOWN = BitShift($KF_ALTDOWN, 8)
Global Const $LLKHF_UP = BitShift($KF_UP, 8)
Func _WinAPI_CreateWindowEx($iExStyle, $sClass, $sName, $iStyle, $iX, $iY, $iWidth, $iHeight, $hParent, $hMenu = 0, $hInstance = 0, $pParam = 0)
If $hInstance = 0 Then $hInstance = _WinAPI_GetModuleHandle("")
Local $aResult = DllCall("user32.dll", "hwnd", "CreateWindowExW", "dword", $iExStyle, "wstr", $sClass, "wstr", $sName, "dword", $iStyle, "int", $iX, "int", $iY, "int", $iWidth, "int", $iHeight, "hwnd", $hParent, "handle", $hMenu, "handle", $hInstance, "ptr", $pParam)
If @error Then Return SetError(@error, @extended, 0)
Return $aResult[0]
EndFunc
Func _WinAPI_GetClassName($hWnd)
If Not IsHWnd($hWnd) Then $hWnd = GUICtrlGetHandle($hWnd)
Local $aResult = DllCall("user32.dll", "int", "GetClassNameW", "hwnd", $hWnd, "wstr", "", "int", 4096)
If @error Then Return SetError(@error, @extended, False)
Return SetExtended($aResult[0], $aResult[2])
EndFunc
Func _WinAPI_GetCurrentThread()
Local $aResult = DllCall("kernel32.dll", "handle", "GetCurrentThread")
If @error Then Return SetError(@error, @extended, 0)
Return $aResult[0]
EndFunc
Func _WinAPI_GetModuleHandle($sModuleName)
Local $sModuleNameType = "wstr"
If $sModuleName = "" Then
$sModuleName = 0
$sModuleNameType = "ptr"
EndIf
Local $aResult = DllCall("kernel32.dll", "handle", "GetModuleHandleW", $sModuleNameType, $sModuleName)
If @error Then Return SetError(@error, @extended, 0)
Return $aResult[0]
EndFunc
Func _WinAPI_GetWindowThreadProcessId($hWnd, ByRef $iPID)
Local $aResult = DllCall("user32.dll", "dword", "GetWindowThreadProcessId", "hwnd", $hWnd, "dword*", 0)
If @error Then Return SetError(@error, @extended, 0)
$iPID = $aResult[2]
Return $aResult[0]
EndFunc
Func _WinAPI_InProcess($hWnd, ByRef $hLastWnd)
If $hWnd = $hLastWnd Then Return True
For $iI = $__gaInProcess_WinAPI[0][0] To 1 Step -1
If $hWnd = $__gaInProcess_WinAPI[$iI][0] Then
If $__gaInProcess_WinAPI[$iI][1] Then
$hLastWnd = $hWnd
Return True
Else
Return False
EndIf
EndIf
Next
Local $iProcessID
_WinAPI_GetWindowThreadProcessId($hWnd, $iProcessID)
Local $iCount = $__gaInProcess_WinAPI[0][0] + 1
If $iCount >= 64 Then $iCount = 1
$__gaInProcess_WinAPI[0][0] = $iCount
$__gaInProcess_WinAPI[$iCount][0] = $hWnd
$__gaInProcess_WinAPI[$iCount][1] =($iProcessID = @AutoItPID)
Return $__gaInProcess_WinAPI[$iCount][1]
EndFunc
Func _WinAPI_IsClassName($hWnd, $sClassName)
Local $sSeparator = Opt("GUIDataSeparatorChar")
Local $aClassName = StringSplit($sClassName, $sSeparator)
If Not IsHWnd($hWnd) Then $hWnd = GUICtrlGetHandle($hWnd)
Local $sClassCheck = _WinAPI_GetClassName($hWnd)
For $x = 1 To UBound($aClassName) - 1
If StringUpper(StringMid($sClassCheck, 1, StringLen($aClassName[$x]))) = StringUpper($aClassName[$x]) Then Return True
Next
Return False
EndFunc
Func _Security__AdjustTokenPrivileges($hToken, $fDisableAll, $pNewState, $iBufferLen, $pPrevState = 0, $pRequired = 0)
Local $aCall = DllCall("advapi32.dll", "bool", "AdjustTokenPrivileges", "handle", $hToken, "bool", $fDisableAll, "struct*", $pNewState, "dword", $iBufferLen, "struct*", $pPrevState, "struct*", $pRequired)
If @error Then Return SetError(1, @extended, False)
Return Not($aCall[0] = 0)
EndFunc
Func _Security__ImpersonateSelf($iLevel = $SECURITYIMPERSONATION)
Local $aCall = DllCall("advapi32.dll", "bool", "ImpersonateSelf", "int", $iLevel)
If @error Then Return SetError(1, @extended, False)
Return Not($aCall[0] = 0)
EndFunc
Func _Security__LookupPrivilegeValue($sSystem, $sName)
Local $aCall = DllCall("advapi32.dll", "bool", "LookupPrivilegeValueW", "wstr", $sSystem, "wstr", $sName, "int64*", 0)
If @error Or Not $aCall[0] Then Return SetError(1, @extended, 0)
Return $aCall[3]
EndFunc
Func _Security__OpenThreadToken($iAccess, $hThread = 0, $fOpenAsSelf = False)
If $hThread = 0 Then $hThread = _WinAPI_GetCurrentThread()
If @error Then Return SetError(1, @extended, 0)
Local $aCall = DllCall("advapi32.dll", "bool", "OpenThreadToken", "handle", $hThread, "dword", $iAccess, "bool", $fOpenAsSelf, "handle*", 0)
If @error Or Not $aCall[0] Then Return SetError(2, @extended, 0)
Return $aCall[4]
EndFunc
Func _Security__OpenThreadTokenEx($iAccess, $hThread = 0, $fOpenAsSelf = False)
Local $hToken = _Security__OpenThreadToken($iAccess, $hThread, $fOpenAsSelf)
If $hToken = 0 Then
If _WinAPI_GetLastError() <> $ERROR_NO_TOKEN Then Return SetError(3, _WinAPI_GetLastError(), 0)
If Not _Security__ImpersonateSelf() Then Return SetError(1, _WinAPI_GetLastError(), 0)
$hToken = _Security__OpenThreadToken($iAccess, $hThread, $fOpenAsSelf)
If $hToken = 0 Then Return SetError(2, _WinAPI_GetLastError(), 0)
EndIf
Return $hToken
EndFunc
Func _Security__SetPrivilege($hToken, $sPrivilege, $fEnable)
Local $iLUID = _Security__LookupPrivilegeValue("", $sPrivilege)
If $iLUID = 0 Then Return SetError(1, @extended, False)
Local $tCurrState = DllStructCreate($tagTOKEN_PRIVILEGES)
Local $iCurrState = DllStructGetSize($tCurrState)
Local $tPrevState = DllStructCreate($tagTOKEN_PRIVILEGES)
Local $iPrevState = DllStructGetSize($tPrevState)
Local $tRequired = DllStructCreate("int Data")
DllStructSetData($tCurrState, "Count", 1)
DllStructSetData($tCurrState, "LUID", $iLUID)
If Not _Security__AdjustTokenPrivileges($hToken, False, $tCurrState, $iCurrState, $tPrevState, $tRequired) Then Return SetError(2, @error, False)
DllStructSetData($tPrevState, "Count", 1)
DllStructSetData($tPrevState, "LUID", $iLUID)
Local $iAttributes = DllStructGetData($tPrevState, "Attributes")
If $fEnable Then
$iAttributes = BitOR($iAttributes, $SE_PRIVILEGE_ENABLED)
Else
$iAttributes = BitAND($iAttributes, BitNOT($SE_PRIVILEGE_ENABLED))
EndIf
DllStructSetData($tPrevState, "Attributes", $iAttributes)
If Not _Security__AdjustTokenPrivileges($hToken, False, $tPrevState, $iPrevState, $tCurrState, $tRequired) Then Return SetError(3, @error, False)
Return True
EndFunc
Global Const $tagMEMMAP = "handle hProc;ulong_ptr Size;ptr Mem"
Func _MemFree(ByRef $tMemMap)
Local $pMemory = DllStructGetData($tMemMap, "Mem")
Local $hProcess = DllStructGetData($tMemMap, "hProc")
Local $bResult = _MemVirtualFreeEx($hProcess, $pMemory, 0, $MEM_RELEASE)
DllCall("kernel32.dll", "bool", "CloseHandle", "handle", $hProcess)
If @error Then Return SetError(@error, @extended, False)
Return $bResult
EndFunc
Func _MemInit($hWnd, $iSize, ByRef $tMemMap)
Local $aResult = DllCall("User32.dll", "dword", "GetWindowThreadProcessId", "hwnd", $hWnd, "dword*", 0)
If @error Then Return SetError(@error, @extended, 0)
Local $iProcessID = $aResult[2]
If $iProcessID = 0 Then Return SetError(1, 0, 0)
Local $iAccess = BitOR($PROCESS_VM_OPERATION, $PROCESS_VM_READ, $PROCESS_VM_WRITE)
Local $hProcess = __Mem_OpenProcess($iAccess, False, $iProcessID, True)
Local $iAlloc = BitOR($MEM_RESERVE, $MEM_COMMIT)
Local $pMemory = _MemVirtualAllocEx($hProcess, 0, $iSize, $iAlloc, $PAGE_READWRITE)
If $pMemory = 0 Then Return SetError(2, 0, 0)
$tMemMap = DllStructCreate($tagMEMMAP)
DllStructSetData($tMemMap, "hProc", $hProcess)
DllStructSetData($tMemMap, "Size", $iSize)
DllStructSetData($tMemMap, "Mem", $pMemory)
Return $pMemory
EndFunc
Func _MemWrite(ByRef $tMemMap, $pSrce, $pDest = 0, $iSize = 0, $sSrce = "struct*")
If $pDest = 0 Then $pDest = DllStructGetData($tMemMap, "Mem")
If $iSize = 0 Then $iSize = DllStructGetData($tMemMap, "Size")
Local $aResult = DllCall("kernel32.dll", "bool", "WriteProcessMemory", "handle", DllStructGetData($tMemMap, "hProc"), "ptr", $pDest, $sSrce, $pSrce, "ulong_ptr", $iSize, "ulong_ptr*", 0)
If @error Then Return SetError(@error, @extended, False)
Return $aResult[0]
EndFunc
Func _MemVirtualAllocEx($hProcess, $pAddress, $iSize, $iAllocation, $iProtect)
Local $aResult = DllCall("kernel32.dll", "ptr", "VirtualAllocEx", "handle", $hProcess, "ptr", $pAddress, "ulong_ptr", $iSize, "dword", $iAllocation, "dword", $iProtect)
If @error Then Return SetError(@error, @extended, 0)
Return $aResult[0]
EndFunc
Func _MemVirtualFreeEx($hProcess, $pAddress, $iSize, $iFreeType)
Local $aResult = DllCall("kernel32.dll", "bool", "VirtualFreeEx", "handle", $hProcess, "ptr", $pAddress, "ulong_ptr", $iSize, "dword", $iFreeType)
If @error Then Return SetError(@error, @extended, False)
Return $aResult[0]
EndFunc
Func __Mem_OpenProcess($iAccess, $fInherit, $iProcessID, $fDebugPriv = False)
Local $aResult = DllCall("kernel32.dll", "handle", "OpenProcess", "dword", $iAccess, "bool", $fInherit, "dword", $iProcessID)
If @error Then Return SetError(@error, @extended, 0)
If $aResult[0] Then Return $aResult[0]
If Not $fDebugPriv Then Return 0
Local $hToken = _Security__OpenThreadTokenEx(BitOR($TOKEN_ADJUST_PRIVILEGES, $TOKEN_QUERY))
If @error Then Return SetError(@error, @extended, 0)
_Security__SetPrivilege($hToken, "SeDebugPrivilege", True)
Local $iError = @error
Local $iLastError = @extended
Local $iRet = 0
If Not @error Then
$aResult = DllCall("kernel32.dll", "handle", "OpenProcess", "dword", $iAccess, "bool", $fInherit, "dword", $iProcessID)
$iError = @error
$iLastError = @extended
If $aResult[0] Then $iRet = $aResult[0]
_Security__SetPrivilege($hToken, "SeDebugPrivilege", False)
If @error Then
$iError = @error
$iLastError = @extended
EndIf
EndIf
DllCall("kernel32.dll", "bool", "CloseHandle", "handle", $hToken)
Return SetError($iError, $iLastError, $iRet)
EndFunc
Global Const $_UDF_GlobalIDs_OFFSET = 2
Global Const $_UDF_GlobalID_MAX_WIN = 16
Global Const $_UDF_STARTID = 10000
Global Const $_UDF_GlobalID_MAX_IDS = 55535
Global Const $__UDFGUICONSTANT_WS_VISIBLE = 0x10000000
Global Const $__UDFGUICONSTANT_WS_CHILD = 0x40000000
Global $_UDF_GlobalIDs_Used[$_UDF_GlobalID_MAX_WIN][$_UDF_GlobalID_MAX_IDS + $_UDF_GlobalIDs_OFFSET + 1]
Func __UDF_GetNextGlobalID($hWnd)
Local $nCtrlID, $iUsedIndex = -1, $fAllUsed = True
If Not WinExists($hWnd) Then Return SetError(-1, -1, 0)
For $iIndex = 0 To $_UDF_GlobalID_MAX_WIN - 1
If $_UDF_GlobalIDs_Used[$iIndex][0] <> 0 Then
If Not WinExists($_UDF_GlobalIDs_Used[$iIndex][0]) Then
For $x = 0 To UBound($_UDF_GlobalIDs_Used, 2) - 1
$_UDF_GlobalIDs_Used[$iIndex][$x] = 0
Next
$_UDF_GlobalIDs_Used[$iIndex][1] = $_UDF_STARTID
$fAllUsed = False
EndIf
EndIf
Next
For $iIndex = 0 To $_UDF_GlobalID_MAX_WIN - 1
If $_UDF_GlobalIDs_Used[$iIndex][0] = $hWnd Then
$iUsedIndex = $iIndex
ExitLoop
EndIf
Next
If $iUsedIndex = -1 Then
For $iIndex = 0 To $_UDF_GlobalID_MAX_WIN - 1
If $_UDF_GlobalIDs_Used[$iIndex][0] = 0 Then
$_UDF_GlobalIDs_Used[$iIndex][0] = $hWnd
$_UDF_GlobalIDs_Used[$iIndex][1] = $_UDF_STARTID
$fAllUsed = False
$iUsedIndex = $iIndex
ExitLoop
EndIf
Next
EndIf
If $iUsedIndex = -1 And $fAllUsed Then Return SetError(16, 0, 0)
If $_UDF_GlobalIDs_Used[$iUsedIndex][1] = $_UDF_STARTID + $_UDF_GlobalID_MAX_IDS Then
For $iIDIndex = $_UDF_GlobalIDs_OFFSET To UBound($_UDF_GlobalIDs_Used, 2) - 1
If $_UDF_GlobalIDs_Used[$iUsedIndex][$iIDIndex] = 0 Then
$nCtrlID =($iIDIndex - $_UDF_GlobalIDs_OFFSET) + 10000
$_UDF_GlobalIDs_Used[$iUsedIndex][$iIDIndex] = $nCtrlID
Return $nCtrlID
EndIf
Next
Return SetError(-1, $_UDF_GlobalID_MAX_IDS, 0)
EndIf
$nCtrlID = $_UDF_GlobalIDs_Used[$iUsedIndex][1]
$_UDF_GlobalIDs_Used[$iUsedIndex][1] += 1
$_UDF_GlobalIDs_Used[$iUsedIndex][($nCtrlID - 10000) + $_UDF_GlobalIDs_OFFSET] = $nCtrlID
Return $nCtrlID
EndFunc
Func __UDF_DebugPrint($sText, $iLine = @ScriptLineNumber, $err = @error, $ext = @extended)
ConsoleWrite( "!===========================================================" & @CRLF & "+======================================================" & @CRLF & "-->Line(" & StringFormat("%04d", $iLine) & "):" & @TAB & $sText & @CRLF & "+======================================================" & @CRLF)
Return SetError($err, $ext, 1)
EndFunc
Func __UDF_ValidateClassName($hWnd, $sClassNames)
__UDF_DebugPrint("This is for debugging only, set the debug variable to false before submitting")
If _WinAPI_IsClassName($hWnd, $sClassNames) Then Return True
Local $sSeparator = Opt("GUIDataSeparatorChar")
$sClassNames = StringReplace($sClassNames, $sSeparator, ",")
__UDF_DebugPrint("Invalid Class Type(s):" & @LF & @TAB & "Expecting Type(s): " & $sClassNames & @LF & @TAB & "Received Type : " & _WinAPI_GetClassName($hWnd))
Exit
EndFunc
Global Const $SS_NOTIFY = 0x0100
Global Const $GUI_SS_DEFAULT_PIC = $SS_NOTIFY
Global Const $__STATUSBARCONSTANT_WM_USER = 0X400
Global Const $SB_GETUNICODEFORMAT = 0x2000 + 6
Global Const $SB_ISSIMPLE =($__STATUSBARCONSTANT_WM_USER + 14)
Global Const $SB_SETPARTS =($__STATUSBARCONSTANT_WM_USER + 4)
Global Const $SB_SETTEXTA =($__STATUSBARCONSTANT_WM_USER + 1)
Global Const $SB_SETTEXTW =($__STATUSBARCONSTANT_WM_USER + 11)
Global Const $SB_SETTEXT = $SB_SETTEXTA
Global Const $SB_SIMPLEID = 0xff
Global $__ghSBLastWnd
Global $Debug_SB = False
Global Const $__STATUSBARCONSTANT_ClassName = "msctls_statusbar32"
Global Const $__STATUSBARCONSTANT_WM_SIZE = 0x05
Func _GUICtrlStatusBar_Create($hWnd, $vPartEdge = -1, $vPartText = "", $iStyles = -1, $iExStyles = -1)
If Not IsHWnd($hWnd) Then Return SetError(1, 0, 0)
Local $iStyle = BitOR($__UDFGUICONSTANT_WS_CHILD, $__UDFGUICONSTANT_WS_VISIBLE)
If $iStyles = -1 Then $iStyles = 0x00000000
If $iExStyles = -1 Then $iExStyles = 0x00000000
Local $aPartWidth[1], $aPartText[1]
If @NumParams > 1 Then
If IsArray($vPartEdge) Then
$aPartWidth = $vPartEdge
Else
$aPartWidth[0] = $vPartEdge
EndIf
If @NumParams = 2 Then
ReDim $aPartText[UBound($aPartWidth)]
Else
If IsArray($vPartText) Then
$aPartText = $vPartText
Else
$aPartText[0] = $vPartText
EndIf
If UBound($aPartWidth) <> UBound($aPartText) Then
Local $iLast
If UBound($aPartWidth) > UBound($aPartText) Then
$iLast = UBound($aPartText)
ReDim $aPartText[UBound($aPartWidth)]
For $x = $iLast To UBound($aPartText) - 1
$aPartWidth[$x] = ""
Next
Else
$iLast = UBound($aPartWidth)
ReDim $aPartWidth[UBound($aPartText)]
For $x = $iLast To UBound($aPartWidth) - 1
$aPartWidth[$x] = $aPartWidth[$x - 1] + 75
Next
$aPartWidth[UBound($aPartText) - 1] = -1
EndIf
EndIf
EndIf
If Not IsHWnd($hWnd) Then $hWnd = HWnd($hWnd)
If @NumParams > 3 Then $iStyle = BitOR($iStyle, $iStyles)
EndIf
Local $nCtrlID = __UDF_GetNextGlobalID($hWnd)
If @error Then Return SetError(@error, @extended, 0)
Local $hWndSBar = _WinAPI_CreateWindowEx($iExStyles, $__STATUSBARCONSTANT_ClassName, "", $iStyle, 0, 0, 0, 0, $hWnd, $nCtrlID)
If @error Then Return SetError(@error, @extended, 0)
If @NumParams > 1 Then
_GUICtrlStatusBar_SetParts($hWndSBar, UBound($aPartWidth), $aPartWidth)
For $x = 0 To UBound($aPartText) - 1
_GUICtrlStatusBar_SetText($hWndSBar, $aPartText[$x], $x)
Next
EndIf
Return $hWndSBar
EndFunc
Func _GUICtrlStatusBar_GetUnicodeFormat($hWnd)
If $Debug_SB Then __UDF_ValidateClassName($hWnd, $__STATUSBARCONSTANT_ClassName)
Return _SendMessage($hWnd, $SB_GETUNICODEFORMAT) <> 0
EndFunc
Func _GUICtrlStatusBar_IsSimple($hWnd)
If $Debug_SB Then __UDF_ValidateClassName($hWnd, $__STATUSBARCONSTANT_ClassName)
Return _SendMessage($hWnd, $SB_ISSIMPLE) <> 0
EndFunc
Func _GUICtrlStatusBar_Resize($hWnd)
If $Debug_SB Then __UDF_ValidateClassName($hWnd, $__STATUSBARCONSTANT_ClassName)
_SendMessage($hWnd, $__STATUSBARCONSTANT_WM_SIZE)
EndFunc
Func _GUICtrlStatusBar_SetParts($hWnd, $iaParts = -1, $iaPartWidth = 25)
If $Debug_SB Then __UDF_ValidateClassName($hWnd, $__STATUSBARCONSTANT_ClassName)
Local $tParts, $iParts = 1
If IsArray($iaParts) <> 0 Then
$iaParts[UBound($iaParts) - 1] = -1
$iParts = UBound($iaParts)
$tParts = DllStructCreate("int[" & $iParts & "]")
For $x = 0 To $iParts - 2
DllStructSetData($tParts, 1, $iaParts[$x], $x + 1)
Next
DllStructSetData($tParts, 1, -1, $iParts)
ElseIf IsArray($iaPartWidth) <> 0 Then
$iParts = UBound($iaPartWidth)
$tParts = DllStructCreate("int[" & $iParts & "]")
For $x = 0 To $iParts - 2
DllStructSetData($tParts, 1, $iaPartWidth[$x], $x + 1)
Next
DllStructSetData($tParts, 1, -1, $iParts)
ElseIf $iaParts > 1 Then
$iParts = $iaParts
$tParts = DllStructCreate("int[" & $iParts & "]")
For $x = 1 To $iParts - 1
DllStructSetData($tParts, 1, $iaPartWidth * $x, $x)
Next
DllStructSetData($tParts, 1, -1, $iParts)
Else
$tParts = DllStructCreate("int")
DllStructSetData($tParts, $iParts, -1)
EndIf
If _WinAPI_InProcess($hWnd, $__ghSBLastWnd) Then
_SendMessage($hWnd, $SB_SETPARTS, $iParts, $tParts, 0, "wparam", "struct*")
Else
Local $iSize = DllStructGetSize($tParts)
Local $tMemMap
Local $pMemory = _MemInit($hWnd, $iSize, $tMemMap)
_MemWrite($tMemMap, $tParts)
_SendMessage($hWnd, $SB_SETPARTS, $iParts, $pMemory, 0, "wparam", "ptr")
_MemFree($tMemMap)
EndIf
_GUICtrlStatusBar_Resize($hWnd)
Return True
EndFunc
Func _GUICtrlStatusBar_SetText($hWnd, $sText = "", $iPart = 0, $iUFlag = 0)
If $Debug_SB Then __UDF_ValidateClassName($hWnd, $__STATUSBARCONSTANT_ClassName)
Local $fUnicode = _GUICtrlStatusBar_GetUnicodeFormat($hWnd)
Local $iBuffer = StringLen($sText) + 1
Local $tText
If $fUnicode Then
$tText = DllStructCreate("wchar Text[" & $iBuffer & "]")
$iBuffer *= 2
Else
$tText = DllStructCreate("char Text[" & $iBuffer & "]")
EndIf
DllStructSetData($tText, "Text", $sText)
If _GUICtrlStatusBar_IsSimple($hWnd) Then $iPart = $SB_SIMPLEID
Local $iRet
If _WinAPI_InProcess($hWnd, $__ghSBLastWnd) Then
$iRet = _SendMessage($hWnd, $SB_SETTEXTW, BitOR($iPart, $iUFlag), $tText, 0, "wparam", "struct*")
Else
Local $tMemMap
Local $pMemory = _MemInit($hWnd, $iBuffer, $tMemMap)
_MemWrite($tMemMap, $tText)
If $fUnicode Then
$iRet = _SendMessage($hWnd, $SB_SETTEXTW, BitOR($iPart, $iUFlag), $pMemory, 0, "wparam", "ptr")
Else
$iRet = _SendMessage($hWnd, $SB_SETTEXT, BitOR($iPart, $iUFlag), $pMemory, 0, "wparam", "ptr")
EndIf
_MemFree($tMemMap)
EndIf
Return $iRet <> 0
EndFunc
Func _ArrayDelete(ByRef $avArray, $iElement)
If Not IsArray($avArray) Then Return SetError(1, 0, 0)
Local $iUBound = UBound($avArray, 1) - 1
If Not $iUBound Then
$avArray = ""
Return 0
EndIf
If $iElement < 0 Then $iElement = 0
If $iElement > $iUBound Then $iElement = $iUBound
Switch UBound($avArray, 0)
Case 1
For $i = $iElement To $iUBound - 1
$avArray[$i] = $avArray[$i + 1]
Next
ReDim $avArray[$iUBound]
Case 2
Local $iSubMax = UBound($avArray, 2) - 1
For $i = $iElement To $iUBound - 1
For $j = 0 To $iSubMax
$avArray[$i][$j] = $avArray[$i + 1][$j]
Next
Next
ReDim $avArray[$iUBound][$iSubMax + 1]
Case Else
Return SetError(3, 0, 0)
EndSwitch
Return $iUBound
EndFunc
Func _ArrayDisplay(Const ByRef $avArray, $sTitle = "Array: ListView Display", $iItemLimit = -1, $iTranspose = 0, $sSeparator = "", $sReplace = "|", $sHeader = "")
If Not IsArray($avArray) Then Return SetError(1, 0, 0)
Local $iDimension = UBound($avArray, 0), $iUBound = UBound($avArray, 1) - 1, $iSubMax = UBound($avArray, 2) - 1
If $iDimension > 2 Then Return SetError(2, 0, 0)
If $sSeparator = "" Then $sSeparator = Chr(124)
If _ArraySearch($avArray, $sSeparator, 0, 0, 0, 1) <> -1 Then
For $x = 1 To 255
If $x >= 32 And $x <= 127 Then ContinueLoop
Local $sFind = _ArraySearch($avArray, Chr($x), 0, 0, 0, 1)
If $sFind = -1 Then
$sSeparator = Chr($x)
ExitLoop
EndIf
Next
EndIf
Local $vTmp, $iBuffer = 4094
Local $iColLimit = 250
Local $iOnEventMode = Opt("GUIOnEventMode", 0), $sDataSeparatorChar = Opt("GUIDataSeparatorChar", $sSeparator)
If $iSubMax < 0 Then $iSubMax = 0
If $iTranspose Then
$vTmp = $iUBound
$iUBound = $iSubMax
$iSubMax = $vTmp
EndIf
If $iSubMax > $iColLimit Then $iSubMax = $iColLimit
If $iItemLimit < 1 Then $iItemLimit = $iUBound
If $iUBound > $iItemLimit Then $iUBound = $iItemLimit
If $sHeader = "" Then
$sHeader = "Row  "
For $i = 0 To $iSubMax
$sHeader &= $sSeparator & "Col " & $i
Next
EndIf
Local $avArrayText[$iUBound + 1]
For $i = 0 To $iUBound
$avArrayText[$i] = "[" & $i & "]"
For $j = 0 To $iSubMax
If $iDimension = 1 Then
If $iTranspose Then
$vTmp = $avArray[$j]
Else
$vTmp = $avArray[$i]
EndIf
Else
If $iTranspose Then
$vTmp = $avArray[$j][$i]
Else
$vTmp = $avArray[$i][$j]
EndIf
EndIf
$vTmp = StringReplace($vTmp, $sSeparator, $sReplace, 0, 1)
If StringLen($vTmp) > $iBuffer Then $vTmp = StringLeft($vTmp, $iBuffer)
$avArrayText[$i] &= $sSeparator & $vTmp
Next
Next
Local Const $_ARRAYCONSTANT_GUI_DOCKBORDERS = 0x66
Local Const $_ARRAYCONSTANT_GUI_DOCKBOTTOM = 0x40
Local Const $_ARRAYCONSTANT_GUI_DOCKHEIGHT = 0x0200
Local Const $_ARRAYCONSTANT_GUI_DOCKLEFT = 0x2
Local Const $_ARRAYCONSTANT_GUI_DOCKRIGHT = 0x4
Local Const $_ARRAYCONSTANT_GUI_EVENT_CLOSE = -3
Local Const $_ARRAYCONSTANT_LVM_GETCOLUMNWIDTH =(0x1000 + 29)
Local Const $_ARRAYCONSTANT_LVM_GETITEMCOUNT =(0x1000 + 4)
Local Const $_ARRAYCONSTANT_LVM_GETITEMSTATE =(0x1000 + 44)
Local Const $_ARRAYCONSTANT_LVM_SETEXTENDEDLISTVIEWSTYLE =(0x1000 + 54)
Local Const $_ARRAYCONSTANT_LVS_EX_FULLROWSELECT = 0x20
Local Const $_ARRAYCONSTANT_LVS_EX_GRIDLINES = 0x1
Local Const $_ARRAYCONSTANT_LVS_SHOWSELALWAYS = 0x8
Local Const $_ARRAYCONSTANT_WS_EX_CLIENTEDGE = 0x0200
Local Const $_ARRAYCONSTANT_WS_MAXIMIZEBOX = 0x00010000
Local Const $_ARRAYCONSTANT_WS_MINIMIZEBOX = 0x00020000
Local Const $_ARRAYCONSTANT_WS_SIZEBOX = 0x00040000
Local $iWidth = 640, $iHeight = 480
Local $hGUI = GUICreate($sTitle, $iWidth, $iHeight, Default, Default, BitOR($_ARRAYCONSTANT_WS_SIZEBOX, $_ARRAYCONSTANT_WS_MINIMIZEBOX, $_ARRAYCONSTANT_WS_MAXIMIZEBOX))
Local $aiGUISize = WinGetClientSize($hGUI)
Local $hListView = GUICtrlCreateListView($sHeader, 0, 0, $aiGUISize[0], $aiGUISize[1] - 26, $_ARRAYCONSTANT_LVS_SHOWSELALWAYS)
Local $hCopy = GUICtrlCreateButton("Copy Selected", 3, $aiGUISize[1] - 23, $aiGUISize[0] - 6, 20)
GUICtrlSetResizing($hListView, $_ARRAYCONSTANT_GUI_DOCKBORDERS)
GUICtrlSetResizing($hCopy, $_ARRAYCONSTANT_GUI_DOCKLEFT + $_ARRAYCONSTANT_GUI_DOCKRIGHT + $_ARRAYCONSTANT_GUI_DOCKBOTTOM + $_ARRAYCONSTANT_GUI_DOCKHEIGHT)
GUICtrlSendMsg($hListView, $_ARRAYCONSTANT_LVM_SETEXTENDEDLISTVIEWSTYLE, $_ARRAYCONSTANT_LVS_EX_GRIDLINES, $_ARRAYCONSTANT_LVS_EX_GRIDLINES)
GUICtrlSendMsg($hListView, $_ARRAYCONSTANT_LVM_SETEXTENDEDLISTVIEWSTYLE, $_ARRAYCONSTANT_LVS_EX_FULLROWSELECT, $_ARRAYCONSTANT_LVS_EX_FULLROWSELECT)
GUICtrlSendMsg($hListView, $_ARRAYCONSTANT_LVM_SETEXTENDEDLISTVIEWSTYLE, $_ARRAYCONSTANT_WS_EX_CLIENTEDGE, $_ARRAYCONSTANT_WS_EX_CLIENTEDGE)
For $i = 0 To $iUBound
GUICtrlCreateListViewItem($avArrayText[$i], $hListView)
Next
$iWidth = 0
For $i = 0 To $iSubMax + 1
$iWidth += GUICtrlSendMsg($hListView, $_ARRAYCONSTANT_LVM_GETCOLUMNWIDTH, $i, 0)
Next
If $iWidth < 250 Then $iWidth = 230
$iWidth += 20
If $iWidth > @DesktopWidth Then $iWidth = @DesktopWidth - 100
WinMove($hGUI, "",(@DesktopWidth - $iWidth) / 2, Default, $iWidth)
GUISetState(@SW_SHOW, $hGUI)
While 1
Switch GUIGetMsg()
Case $_ARRAYCONSTANT_GUI_EVENT_CLOSE
ExitLoop
Case $hCopy
Local $sClip = ""
Local $aiCurItems[1] = [0]
For $i = 0 To GUICtrlSendMsg($hListView, $_ARRAYCONSTANT_LVM_GETITEMCOUNT, 0, 0)
If GUICtrlSendMsg($hListView, $_ARRAYCONSTANT_LVM_GETITEMSTATE, $i, 0x2) Then
$aiCurItems[0] += 1
ReDim $aiCurItems[$aiCurItems[0] + 1]
$aiCurItems[$aiCurItems[0]] = $i
EndIf
Next
If Not $aiCurItems[0] Then
For $sItem In $avArrayText
$sClip &= $sItem & @CRLF
Next
Else
For $i = 1 To UBound($aiCurItems) - 1
$sClip &= $avArrayText[$aiCurItems[$i]] & @CRLF
Next
EndIf
ClipPut($sClip)
EndSwitch
WEnd
GUIDelete($hGUI)
Opt("GUIOnEventMode", $iOnEventMode)
Opt("GUIDataSeparatorChar", $sDataSeparatorChar)
Return 1
EndFunc
Func _ArrayPush(ByRef $avArray, $vValue, $iDirection = 0)
If(Not IsArray($avArray)) Then Return SetError(1, 0, 0)
If UBound($avArray, 0) <> 1 Then Return SetError(3, 0, 0)
Local $iUBound = UBound($avArray) - 1
If IsArray($vValue) Then
Local $iUBoundS = UBound($vValue)
If($iUBoundS - 1) > $iUBound Then Return SetError(2, 0, 0)
If $iDirection Then
For $i = $iUBound To $iUBoundS Step -1
$avArray[$i] = $avArray[$i - $iUBoundS]
Next
For $i = 0 To $iUBoundS - 1
$avArray[$i] = $vValue[$i]
Next
Else
For $i = 0 To $iUBound - $iUBoundS
$avArray[$i] = $avArray[$i + $iUBoundS]
Next
For $i = 0 To $iUBoundS - 1
$avArray[$i + $iUBound - $iUBoundS + 1] = $vValue[$i]
Next
EndIf
Else
If $iDirection Then
For $i = $iUBound To 1 Step -1
$avArray[$i] = $avArray[$i - 1]
Next
$avArray[0] = $vValue
Else
For $i = 0 To $iUBound - 1
$avArray[$i] = $avArray[$i + 1]
Next
$avArray[$iUBound] = $vValue
EndIf
EndIf
Return 1
EndFunc
Func _ArraySearch(Const ByRef $avArray, $vValue, $iStart = 0, $iEnd = 0, $iCase = 0, $iCompare = 0, $iForward = 1, $iSubItem = -1)
If Not IsArray($avArray) Then Return SetError(1, 0, -1)
If UBound($avArray, 0) > 2 Or UBound($avArray, 0) < 1 Then Return SetError(2, 0, -1)
Local $iUBound = UBound($avArray) - 1
If $iEnd < 1 Or $iEnd > $iUBound Then $iEnd = $iUBound
If $iStart < 0 Then $iStart = 0
If $iStart > $iEnd Then Return SetError(4, 0, -1)
Local $iStep = 1
If Not $iForward Then
Local $iTmp = $iStart
$iStart = $iEnd
$iEnd = $iTmp
$iStep = -1
EndIf
Local $iCompType = False
If $iCompare = 2 Then
$iCompare = 0
$iCompType = True
EndIf
Switch UBound($avArray, 0)
Case 1
If Not $iCompare Then
If Not $iCase Then
For $i = $iStart To $iEnd Step $iStep
If $iCompType And VarGetType($avArray[$i]) <> VarGetType($vValue) Then ContinueLoop
If $avArray[$i] = $vValue Then Return $i
Next
Else
For $i = $iStart To $iEnd Step $iStep
If $iCompType And VarGetType($avArray[$i]) <> VarGetType($vValue) Then ContinueLoop
If $avArray[$i] == $vValue Then Return $i
Next
EndIf
Else
For $i = $iStart To $iEnd Step $iStep
If StringInStr($avArray[$i], $vValue, $iCase) > 0 Then Return $i
Next
EndIf
Case 2
Local $iUBoundSub = UBound($avArray, 2) - 1
If $iSubItem > $iUBoundSub Then $iSubItem = $iUBoundSub
If $iSubItem < 0 Then
$iSubItem = 0
Else
$iUBoundSub = $iSubItem
EndIf
For $j = $iSubItem To $iUBoundSub
If Not $iCompare Then
If Not $iCase Then
For $i = $iStart To $iEnd Step $iStep
If $iCompType And VarGetType($avArray[$i][$j]) <> VarGetType($vValue) Then ContinueLoop
If $avArray[$i][$j] = $vValue Then Return $i
Next
Else
For $i = $iStart To $iEnd Step $iStep
If $iCompType And VarGetType($avArray[$i][$j]) <> VarGetType($vValue) Then ContinueLoop
If $avArray[$i][$j] == $vValue Then Return $i
Next
EndIf
Else
For $i = $iStart To $iEnd Step $iStep
If StringInStr($avArray[$i][$j], $vValue, $iCase) > 0 Then Return $i
Next
EndIf
Next
Case Else
Return SetError(7, 0, -1)
EndSwitch
Return SetError(6, 0, -1)
EndFunc
Global $ghGDIPDll = 0
Global $giGDIPRef = 0
Global $giGDIPToken = 0
Func _GDIPlus_Startup()
$giGDIPRef += 1
If $giGDIPRef > 1 Then Return True
$ghGDIPDll = DllOpen("GDIPlus.dll")
If $ghGDIPDll = -1 Then
$giGDIPRef = 0
Return SetError(1, 2, False)
EndIf
Local $tInput = DllStructCreate($tagGDIPSTARTUPINPUT)
Local $tToken = DllStructCreate("ulong_ptr Data")
DllStructSetData($tInput, "Version", 1)
Local $aResult = DllCall($ghGDIPDll, "int", "GdiplusStartup", "struct*", $tToken, "struct*", $tInput, "ptr", 0)
If @error Then Return SetError(@error, @extended, False)
$giGDIPToken = DllStructGetData($tToken, "Data")
Return $aResult[0] = 0
EndFunc
_GDIPlus_Startup()
Global $mgdebug = false
Const $sUDFVersion = 'CommMG.au3 V2.90'
Global $fPortOpen = False
Global $hDll
Global $DLLNAME = 'commg.dll'
Func _CommListPorts($iReturnType = 1)
Local $vDllAns, $lpres
If Not $fPortOpen Then
$hDll = DllOpen($DLLNAME)
If $hDll = -1 Then
SetError(2)
Return 0
EndIf
$fPortOpen = True
EndIf
If $fPortOpen Then
$vDllAns = DllCall($hDll, 'str', 'ListPorts')
If @error = 1 Then
SetError(1)
Return ''
Else
If $iReturnType = 1 Then
Return $vDllAns[0]
Else
Return StringSplit($vDllAns[0], '|')
EndIf
EndIf
Else
SetError(1)
Return ''
EndIf
EndFunc
Func _CommSetPort($iPort, ByRef $sErr, $iBaud = 9600, $iBits = 8, $iPar = 0, $iStop = 1, $iFlow = 0, $RTSMode = 0, $DTRMode = 0)
Local $vDllAns
$sErr = ''
mgdebugCW("$fPortOpen = " & $fPortOpen & @CRLF)
If Not $fPortOpen Then
$hDll = DllOpen($DLLNAME)
If $hDll = -1 Then
SetError(2)
$sErr = 'Failed to open commg.dll'
Return 0
EndIf
$fPortOpen = True
EndIf
mgdebugCW('port = ' & $iPort & ', baud = ' & $iBaud & ', bits = ' & $iBits & ', par = ' & $iPar & ', stop = ' & $iStop & ', flow = ' & $iFlow & @CRLF)
$vDllAns = DllCall($hDll, 'int', 'SetPort', 'int', $iPort, 'int', $iBaud, 'int', $iBits, 'int', $iPar, 'int', $iStop, 'int', $iFlow, 'int', $RTSMode, 'int', $DTRMode)
If @error <> 0 Then
$sErr = 'dll SetPort call failed'
SetError(1)
Return 0
EndIf
If $vDllAns[0] < 0 Then
SetError($vDllAns[0])
Switch $vDllAns[0]
Case -1
$sErr = 'undefined baud rate'
Case -2
$sErr = 'undefined stop bit number'
Case -4
$sErr = 'undefined data size'
Case -8
$sErr = 'port 0 not allowed'
Case -16
$sErr = 'port does not exist'
Case -32
$sErr = 'access denied, maybe port already in use'
Case -64
$sErr = 'unknown error accessing port'
EndSwitch
Return 0
Else
Return 1
EndIf
EndFunc
Func _Commgetstring()
Local $vDllAns
If Not $fPortOpen Then
SetError(1)
Return 0
EndIf
$vDllAns = DllCall($hDll, 'str', 'GetString')
If @error <> 0 Then
SetError(1)
mgdebugCW('error in _commgetstring' & @CRLF)
Return ''
EndIf
Return $vDllAns[0]
EndFunc
Func _CommGetLine($sEndChar = @CR, $maxlen = 0, $maxtime = 0)
Local $vDllAns, $sLineRet, $sStr1, $waited, $sNextChar, $iSaveErr
If Not $fPortOpen Then
SetError(1)
mgdebugCW("Port not open" & @CRLF)
Return 0
EndIf
$sStr1 = ''
$waited = TimerInit()
While 1
If TimerDiff($waited) > $maxtime And $maxtime > 0 Then
SetError(-2)
mgdebugCW("Time Too long" & @CRLF)
Return $sStr1
EndIf
If StringLen($sStr1) >= $maxlen And $maxlen > 0 Then
SetError(-1)
mgdebugCW("String to long" & @CRLF)
Return $sStr1
EndIf
$sNextChar = _CommReadChar()
$iSaveErr = @error
If $iSaveErr <> 0 Then
mgdebugCW("$iSaveErr = " & $iSaveErr & @CRLF)
EndIf
If $iSaveErr = 0 Then
$sStr1 = $sStr1 & $sNextChar
mgdebugCW($sStr1 & @CRLF)
If $sNextChar = $sEndChar Then ExitLoop
EndIf
If $iSaveErr <> 0 And $iSaveErr <> 3 Then
SetError(1)
mgdebugCW("Errors" & @CRLF)
Return $sStr1
EndIf
WEnd
Return $sStr1
EndFunc
Func _CommGetInputCount()
Local $vDllAns
If Not $fPortOpen Then
SetError(1)
Return 0
EndIf
mgdebugCW("to dll getipcount")
$vDllAns = DllCall($hDll, 'str', 'GetInputCount')
If @error <> 0 Then
SetError(1)
Return 0
Else
Return $vDllAns[0]
EndIf
EndFunc
Func _CommReadByte($wait = 0)
Local $iCount, $vDllAns
If Not $fPortOpen Then
SetError(1)
Return ''
EndIf
mgdebugCW("599 before get input count")
If Not $wait Then
$iCount = _CommGetInputCount()
If @error = 1 Or $iCount = 0 Then
SetError(1)
Return ''
EndIf
EndIf
mgdebugCW("to dll getbyte")
$vDllAns = DllCall($hDll, 'str', 'GetByte')
If @error <> 0 Then
mgdebugCW("readbyte error = " & @error & @CRLF)
SetError(2)
Return ''
EndIf
If StringLen($vDllAns[0]) > 3 or $vDllAns[0] > 255 Then
mgdebugCW("getbyte call returned >" & $vDllAns[0] & @CRLF)
Return SetError(3, 0, $vDllAns[0])
EndIf
Return $vDllAns[0]
EndFunc
Func _CommReadChar($wait = 0)
Local $sChar, $iErr
If Not $fPortOpen Then
SetError(1)
Return 0
EndIf
mgdebugCW("will read byte")
$sChar = _CommReadByte($wait)
$iErr = @error
If $iErr > 2 Then
mgdebugCW("$iErr = " & $iErr & @CR)
SetError(1)
Return ''
EndIf
mgdebugCW(Chr(Execute($sChar)) & @CRLF)
If $iErr == 0 Then Return Chr(Execute($sChar))
EndFunc
Func _CommClearOutputBuffer()
If Not $fPortOpen Then
SetError(1)
Return 0
EndIf
Local $vDllAns = DllCall($hDll, 'int', 'ClearOutputBuffer')
EndFunc
Func _CommClearInputBuffer()
If Not $fPortOpen Then
SetError(1)
Return 0
EndIf
Local $vDllAns = DllCall($hDll, 'int', 'ClearInputBuffer')
If @error <> 0 Then
Return -1
Else
Return 1
EndIf
EndFunc
Func _CommClosePort($fAll = False)
Local $closeAll
If Not $fPortOpen Then
SetError(1)
Return 0
EndIf
If $fAll Then
$closeAll = 1
Else
$closeAll = 0
EndIf
_CommClearOutputBuffer()
_CommClearInputBuffer()
DllCall($hDll, 'int', 'CloseDown', 'int', $closeAll)
If @error <> 0 Then ConsoleWrite("Error closing dll" & @CRLF)
$fPortOpen = False
if $fAll then DllClose($hDll)
EndFunc
Func mgdebugCW($sDB)
If Not $mgdebug Then Return
ConsoleWrite($sDB)
EndFunc
Opt("MustDeclareVars", 0)
Opt("TrayMenuMode", 1)
Opt("TrayMenuMode", 0)
Opt("TrayMenuMode", 0)
$Form1 = GUICreate("Form1", 613, 438, 192, 124)
$portbox = GUICtrlCreateList("", 40, 48, 81, 84)
$Button2 = GUICtrlCreateButton("Clear I/Buffer", 280, 40, 75, 25)
$singlestep = GUICtrlCreateButton("Single", 280, 120, 75, 25)
$Button4 = GUICtrlCreateButton("test345", 280, 185, 75, 25)
$baudinput = GUICtrlCreateInput("250000", 168, 56, 73, 21, BitOR($GUI_SS_DEFAULT_INPUT, $ES_RIGHT, $ES_NUMBER))
GUICtrlSetBkColor(-1, 0xBFCDDB)
$connectbutton = GUICtrlCreateButton("Connect", 168, 104, 75, 25, $BS_DEFPUSHBUTTON)
$startstop = GUICtrlCreateButton("Start/Stop", 280, 80, 75, 25, $GUI_DISABLE)
GUIStartGroup()
$Checkbox1 = GUICtrlCreateCheckbox("Clear on read", 40, 200, 97, 17)
$Checkbox2 = GUICtrlCreateCheckbox("Store input to file", 40, 224, 97, 17)
$Checkbox3 = GUICtrlCreateCheckbox("Write Log", 40, 248, 97, 17)
GUICtrlSetState($Checkbox1, $GUI_CHECKED)
$bin = GUICtrlCreateRadio("BIN", 176, 200, 100, 17)
$hex = GUICtrlCreateRadio("HEX", 176, 224, 100, 17)
$dec = GUICtrlCreateRadio("DEC", 176, 248, 100, 17)
GUICtrlSetState($hex, $GUI_CHECKED)
GUIStartGroup()
$Group1 = GUICtrlCreateGroup(" Output Settings ", 24, 176, 241, 105)
GUICtrlCreateGroup("", -99, -99, 1, 1)
$Group2 = GUICtrlCreateGroup(" Main Settings ", 24, 24, 241, 129)
$Label1 = GUICtrlCreateLabel("Baud", 128, 56, 32, 18)
GUICtrlSetFont(-1, 11, 400, 0, "MS Sans Serif")
GUICtrlSetResizing(-1, $GUI_DOCKAUTO)
$refreshlist = GUICtrlCreateButton("rf", 128, 80, 27, 25, $BS_BITMAP)
GUICtrlSetImage(-1, "C:\Users\JannisDesktop\Desktop\RETRY.BMP", -1)
GUICtrlCreateGroup("", -99, -99, 1, 1)
$fileoptions = GUICtrlCreateGroup("File Options", 24, 288, 113, 65)
$totxt = GUICtrlCreateCheckbox("Export to .txt", 40, 304, 89, 17)
$tocsv = GUICtrlCreateCheckbox("Export to Excel", 40, 328, 89, 17)
GUICtrlCreateGroup("", -99, -99, 1, 1)
GUICtrlSetState($tocsv, $GUI_HIDE)
GUICtrlSetState($totxt, $GUI_HIDE)
GUICtrlSetState($fileoptions, $GUI_HIDE)
$Pic1 = GUICtrlCreatePic("C:\Users\JannisDesktop\Desktop\Unbenannt.JPG", 376, 24, 217, 137, BitOR($GUI_SS_DEFAULT_PIC, $GUI_AVISTOP))
$StatusBar1 = _GUICtrlStatusBar_Create($Form1)
Dim $StatusBar1_PartsWidth[3] = [200, 400, -1]
_GUICtrlStatusBar_SetParts($StatusBar1, $StatusBar1_PartsWidth)
_GUICtrlStatusBar_SetText($StatusBar1, "", 0)
_GUICtrlStatusBar_SetText($StatusBar1, "", 1)
_GUICtrlStatusBar_SetText($StatusBar1, "", 2)
TraySetIcon("F:\Program Files\ST-Link-Utility\ST-LINK Utility\ST-LinkUpgrade.exe", -1)
TraySetClick("0")
$MenuItem2 = TrayCreateMenu("Show")
$MenuItem3 = TrayCreateItem("MenuItem3", $MenuItem2)
$MenuItem1 = TrayCreateItem("Hide")
GUISetState(@SW_SHOW)
GUICtrlSetOnEvent($GUI_EVENT_CLOSE, "_Exit")
$portopenErr = ""
$connectbutton_ptr = 0x00
$running_flag = 0
$connected = "Closed"
$i = 0
Global $tim_rec_int = TimerInit()
Global $tim_2 = TimerInit()
$rec_delay = 100
Global $record
GUICtrlSetData($portbox, "")
$portlist = _CommListPorts()
GUICtrlSetData($portbox, $portlist)
While 1
$nMsg = GUIGetMsg()
Switch $nMsg
Case $GUI_EVENT_CLOSE
Exit
Case $refreshlist
GUICtrlSetData($portbox, "")
$portlist = _CommListPorts()
GUICtrlSetData($portbox, $portlist)
Case $Button2
_CommClearInputBuffer()
Case $singlestep
If IsChecked($Checkbox1) Then
_CommClearInputBuffer()
EndIf
ConsoleWrite(StringRegExpReplace(StringTrimLeft(StringToBinary(_CommGetLine(333, 48, 0)), 2), "(..)", "\1 ") & @CRLF)
Case $Button4
$test86 = _Commgetstring()
ConsoleWrite(StringRegExpReplace(StringTrimLeft(StringToBinary($test86), 2), "(..)", "\1 ") & @CRLF)
Case $connectbutton
If GUICtrlRead($connectbutton) = "Connect" Then
_connect2port()
Else
_closeport()
EndIf
Case $startstop
$running_flag = BitXOR($running_flag, $GUI_CHECKED)
EndSwitch
_GUICtrlStatusBar_SetText($StatusBar1, "null", 0)
_GUICtrlStatusBar_SetText($StatusBar1, "Available Packets: " & _CommGetInputCount(), 1)
_GUICtrlStatusBar_SetText($StatusBar1, "Connection Status: " & $connected, 2)
If $connected = "Established" Then
GUICtrlSetState($startstop, $GUI_ENABLE)
GUICtrlSetState($singlestep, $GUI_ENABLE)
Else
GUICtrlSetState($startstop, $GUI_DISABLE)
GUICtrlSetState($singlestep, $GUI_DISABLE)
EndIf
If(IsChecked($Checkbox2)) Then
_toggle_fileoptions()
EndIf
If(TimerDiff($tim_rec_int) >= $rec_delay And $running_flag) Then
$tim_rec_int = TimerInit()
$record = _recv3(10)
ConsoleWrite($record & @CRLF)
_PrintFromArray($record)
$i+=1
ConsoleWrite($i & @CRLF)
Endif
WEnd
Func _recv3(Const $records = 1)
Local $temp_in[$records]
Local $readings[$records][48]
If IsChecked($Checkbox1) Then
_CommClearInputBuffer()
Sleep(9*$records)
EndIf
For $i = 0 To $records-1
$temp_in[$i] = StringRegExpReplace(StringTrimLeft(StringToBinary(_CommGetLine(333, 48, 0)), 2), "(..)", "\1 ")
If(StringLen($temp_in[$i]) < 48) Then
MsgBox(0, "Error", "Received String(s) were empty or too short after 25 retrys" & @CRLF & $temp_in[$i])
Return SetError(1, 0, 0)
EndIf
Next
For $i = 0 To $records-1
$temparray = StringSplit($temp_in[$i], " ")
If Ubound($temparray) > 49 Then
_ArrayDelete($temparray, 49)
EndIf
$temparray = _sortarray($temparray)
For $i2 = 0 To(UBound($temparray))-1
$readings[$i][$i2]=$temparray[($i2)]
Next
Next
Return($readings)
EndFunc
Func _Exit()
Exit
EndFunc
Func _connect2port()
GUICtrlSetData($connectbutton, "Disconnect")
$baudr = GUICtrlRead($baudinput)
Global $comport = Number(StringRight(GUICtrlRead($portbox), 1))
If IsNumber($comport) And $comport > 0 Then
If _CommSetPort($comport, $portopenErr, $baudr, 8, 0, 1, 0, 0, 0) Then
$connected = "Established"
ConsoleWrite("port " & $comport & " opened at " & $baudr & " baud" & @CRLF)
Else
MsgBox(0, "Error", "Could not open Port" & @CRLF & "")
EndIf
Else
MsgBox(0, "Error", "Could not open Port" & @CRLF & "No Port selected or higher than 9?")
EndIf
EndFunc
Func _closeport()
GUICtrlSetData($connectbutton, "Connect")
If Not _CommClosePort() Then
$connected = "Closed"
ConsoleWrite("port " & $comport & " closed." & @CRLF)
Else
MsgBox(0, "Error", "Could not close Port")
EndIf
EndFunc
Func IsChecked(Const ByRef $controlID)
If(BitAND(GUICtrlRead($controlID), $GUI_CHECKED)) Then Return TRUE
Return FALSE
EndFunc
Func _toggle_fileoptions()
If(BitAND(GUICtrlGetState($fileoptions), $GUI_HIDE)) Then
GUICtrlSetState($fileoptions, $GUI_SHOW)
GUICtrlSetState($tocsv, $GUI_SHOW)
GUICtrlSetState($totxt, $GUI_SHOW)
Else
GUICtrlSetState($fileoptions, $GUI_HIDE)
GUICtrlSetState($tocsv, $GUI_HIDE)
GUICtrlSetState($totxt, $GUI_HIDE)
EndIf
EndFunc
Func _sortarray($array)
Local $limiter = 0
If(UBound($array) <> 49) Then
MsgBox(0, "Error", "In _sortarray; array too short" & @CRLF)
$running_flag = 0
_ArrayDisplay($array)
Return SetError(1, 0, 0)
EndIf
_ArrayDelete($array, 0)
While($array[0] <> "FF")
If($limiter > 49) Then
MsgBox(0, "Error", "In _sortarray; no FF found" & @CRLF)
_ArrayDisplay($array)
Return SetError(1, 0, 0)
EndIf
_ArrayPush($array, $array[47], 1)
$limiter += 1
WEnd
Return($array)
EndFunc
Func _PrintFromArray(ByRef Const $aArray, $iBase = Default, $iUBound = Default, $sDelimeter = "|")
If Not IsArray($aArray) Then Return SetError(1, 0, 0)
Local $iDims = UBound($aArray, 0)
If $iDims > 2 Then Return SetError(2, 0, 0)
Local $iLast = UBound($aArray) - 1
If $iUBound = Default Or $iUBound > $iLast Then $iUBound = $iLast
If $iBase < 0 Or $iBase = Default Then $iBase = 0
If $iBase > $iUBound Then Return SetError(3, 0, 0)
If $sDelimeter = Default Then $sDelimeter = "|"
Switch $iDims
Case 1
For $i = $iBase To $iUBound
ConsoleWrite("[" & $i - $iBase & "] " & $aArray[$i] & @CRLF)
Next
Case 2
Local $sTemp = ""
Local $iCols = UBound($aArray, 2)
For $i = $iBase To $iUBound
$sTemp = $aArray[$i][0]
For $j = 1 To $iCols - 1
$sTemp &= $sDelimeter & $aArray[$i][$j]
Next
ConsoleWrite("[" & $i - $iBase & "] " & $sTemp & @CRLF)
Next
EndSwitch
Return 1
EndFunc
